# Map project *cadastre.squat.net*

## About

In France, the *cadastre* is a registry of the state of land ownership at the national level. The official website of the French government where you can consult the digital version of this registry, [cadastre.gouv.fr](https://cadastre.gouv.fr), is quite bad: the search is painful, the map background is unpleasant, and above all it is not possible to consult the owners of the plots.

The present map is meant to be a better online cadastre, for activists, squatters, anarchists and other weirdos. It aims to answer questions like "*Who owns this building?*" or "*Where is this company present in this region?*". It currently references:

 - All cadastral plots and postal addresses in France, both mainland and overseas.
 - Owners and managers of plots, or of premises located on plots.
 - Establishments registered at postal addresses.

With some exceptions, the map only references legal entities (companies, associations, public institutions...) as opposed to private individuals, as information regarding private individuals isn't publicly available for privacy reasons.

To contact the amateur cartographers who maintain this map, send an email to hm123@riseup.net.

## Build

### Step 1: Clone this repository

`git clone https://0xacab.org/squatnet/skwotcustom_containers/cadastre.git`

### Step 2: Download the data

*This step requires an Internet connection and about ~90GB of disk space.*

 - Download the [BANO](https://bano.openstreetmap.fr/data) (Base Adresses Nationale Ouverte) department-specific CSV files (e.g. `bano-01.csv`, `bano-02.csv`) to a directory. You can use the script `python/download_bano.py` (run with `-h` for usage details) to automate the download.
 - Download the [Géo-SIRENE](https://data.cquest.org/geo_sirene/v2019) latest department-specific files (e.g. `geo_siret_01.csv.gz`, `geo_siret_02.csv.gz`) to a directory and extract them. You can use the script `python/download_geo_sirene.py` (run with `-h` for usage details) to automate the download and extraction.
 - Download the latest [Fichiers des locaux des personnes morales](https://data.economie.gouv.fr/explore/dataset/fichiers-des-locaux-et-des-parcelles-des-personnes-morales) to a directory and extract them.
 - Download the latest [Fichiers des parcelles des personnes morales](https://data.economie.gouv.fr/explore/dataset/fichiers-des-locaux-et-des-parcelles-des-personnes-morales) to a directory and extract them.
 - Download the latest Parcellaire Express database from [here](https://geoservices.ign.fr/parcellaire-express-pci) or [here](https://data.cquest.org/ign/parcellaire-express) to a directory and extract it. You can use the script `python/download_parcellaire_express.py` (run with `-h` for usage details) to automate the download but you will need to extract manually (for example with a command such as `7za x "*.7z"`).
 - Download the latest [SIRENE "Unités légales (données courantes)"](https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret) database to a directory and extract it.

### Step 3: Create the configuration file

Create a `config.ini` file at the repository root with the following contents:

```
[bano]
path = <PATH>

[geo-sirene]
path = <PATH>
year = <YEAR>
month = <MONTH>

[fichiers-des-locaux]
path = <PATH>
year = <YEAR>
month = <MONTH>

[fichiers-des-parcelles]
path = <PATH>
year = <YEAR>
month = <MONTH>

[parcellaire-express]
path = <PATH>

[sirene-unites-legales]
path = <PATH>
```

Then:

 - Replace each instance of `<PATH>` with the path where you downloaded the corresponding data. Relative paths will be interpreted as relative to the directory from which you will later execute the script to generate the database.
 - Replace each instance of `<YEAR>` with the year in which the corresponding data was produced (e.g. `2023`), and each instance of `<MONTH>` with the month in which the corresponding data was produced (e.g. `2`). These attributes should be given by the website from which you downloaded the data.

### Step 4: Generate the database

*This step requires about ~180GB of disk space (including the disk space of the downloaded data), about ~12GB of RAM, and can take several days depending on your hardware.*

 - Install required software, e.g. on Debian `apt install python3-gdal python3-unidecode libsqlite3-mod-spatialite`.
 - Run the Python script `python/database.py` (run with `-h` for usage details).
 - Make sure the database file has the correct permissions: run `chmod a+r <FILE>` where `<FILE>` is the database file.

### Step 5: Pull and build container images

 - Install podman (e.g. on Debian `apt install podman`).
 - Run `podman-build.sh`.

### Step 6: Run container images

Run `podman-run.sh` (run without arguments for usage details).

### Step 7: The map should work now

Test that the map works fine, e.g. by opening `https://127.0.0.1:65000` in a local browser.

**Note:** to access `127.0.0.1` in Tor Browser, you need to change `network.proxy.allow_hijacking_localhost` to `false` in Tor Browser's `about:config`.

## Screenshots

![ ](screenshot-1.png)

![ ](screenshot-2.png)

![ ](screenshot-3.png)
