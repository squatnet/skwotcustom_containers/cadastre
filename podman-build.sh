#!/bin/sh

cd "$(dirname "$0")"
set -e

# Pull nginx image
podman pull docker.io/library/nginx:stable-alpine

# Build php-fpm image
podman build --no-cache \
		-f ./php-fpm/Containerfile \
		--tag cadastre-php-fpm
