#!/bin/sh

cd "$(dirname "$0")"
set -e

# Print usage
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 DATABASE_PATH"
    echo "The database path should be either absolute or relative to the script directory."
    exit 1
fi

# Set variables
POD_NAME=cadastre
POD_PUBLISHED_PORT=65000

# Create pod
podman pod create \
        --name "$POD_NAME" \
        --replace \
        --publish 127.0.0.1:${POD_PUBLISHED_PORT}:80

# Nginx
podman run \
        --pod "$POD_NAME" \
        --name nginx \
        --replace \
        --detach \
        --volume "./www:/var/www/html:ro" \
        --volume "./nginx/nginx.conf:/etc/nginx/conf.d/default.conf:ro" \
        docker.io/library/nginx:stable-alpine

# PHP-fpm
podman run \
        --pod "$POD_NAME" \
        --name php-fpm \
        --replace \
        --detach \
        --volume "./php-fpm/php.ini:/usr/local/etc/php/php.ini:ro" \
        --volume "./www:/var/www/html:ro" \
        --volume "$1:/var/www/database.db:ro" \
        cadastre-php-fpm
