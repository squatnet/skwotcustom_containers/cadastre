from __future__ import annotations

import configparser
import csv
import logging
import osgeo.ogr
import pathlib
import re
import typing

import util

class Database:
    """Handles the BANO (Base Adresses Nationale Ouverte) database."""
    
    # These departments are not covered by the BANO
    EXCLUDED_DEPARTMENTS = ["977", "978"]
    
    def __init__(self, config: configparser.SectionProxy):
        """
        :param config: Configuration read from the configuration file.
        """
        
        self.config = config
        
    def departement(self, departement: util.Departement) -> dict[util.AddressKey, Address]:
        """Returns all adresses from a departement."""
        
        # Addresses
        addresses: dict[util.AddressKey, Address] = {}
        
        # Number of addresses that could not be loaded
        unloadable_addresses = 0
        
        # Load addresses
        for row in self.rows(departement):
            # Add the address
            address_key = util.AddressKey(
                f"{row[1]} {row[2]}",
                util.Commune(row[0][0:5])
            )
            if address_key not in addresses:
                addresses[address_key] = Address(
                    address_key,
                    departement,
                    f"{row[1]} {row[2]}, {row[3]} {row[4]}",
                    float(row[6]),
                    float(row[7])
                )
            else:
                unloadable_addresses += 1
                    
        # Log message
        if addresses:
            if unloadable_addresses != 0:
                error_ratio = unloadable_addresses / (unloadable_addresses + len(addresses))
                logging.warning(f"Unable to load {unloadable_addresses}/{unloadable_addresses + len(addresses)} addresses from the BANO database for departement \"{departement}\" (error rate: {error_ratio * 100:.4f}%, expected ~0.5%).")
            
        # Return addresses
        return addresses
        
    def rows(self, department: util.Departement):
        """Generator function which yields all rows for a given department."""
        
        if department.code not in self.EXCLUDED_DEPARTMENTS:
            csv_path: typing.Union[pathlib.Path, None] = None
            for path in pathlib.Path(self.config["path"]).rglob("*"):
                if path.name == f"bano-{department.code}.csv" and path.is_file():
                    csv_path = path
                    break
            if csv_path == None:
                raise RuntimeError(f"No file found in the BANO database for department \"{department}\".")
            with path.open("r", newline = "") as csv_file:
                csv_reader = csv.reader(csv_file, delimiter = ",")
                for row in csv_reader:
                    yield row

class Address:
    """Address."""
    
    def __init__(self, key: util.AddressKey, departement: util.Departement, libelle: str, latitude: float, longitude: float):
        """
        :param key: Address key.
        :param departement: Address departement.
        :param libelle: Address "libellé", e.g. "123 Rue du Lac".
        :param latitude: Address point latitude.
        :param longitude: Address point longitude.
        """
        
        self.key = key
        self.departement = departement
        self.libelle = libelle
        self.latitude = latitude
        self.longitude = longitude
        
        # Geography
        point = osgeo.ogr.Geometry(osgeo.ogr.wkbPoint)
        point.AddPoint_2D(self.latitude, self.longitude)
        self.geography = str(point)
