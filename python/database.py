from __future__ import annotations

import argparse
import configparser
import csv
import dataclasses
import json
import logging
import os
import osgeo.ogr
import pathlib
import re
import sqlite3
import typing
import unidecode

import bano
import geo_sirene
import fichiers_personnes_morales
import parcellaire_express
import sirene_unites_legales
import util

class Database:
    """
    Fills a SQLite database with all French land plots and addresses, including information on entities
    that own or manage the plots/addresses where available.
    """
    
    # Minimum number of locations an entity should be related to to be considered a "big" entity (in addition to being related to locations in several departements)
    # Users cannot search locations related to big entities across all France and must instead search in specific departements
    BIG_ENTITY_MINIMUM_LOCATIONS = 5000
    
    def __init__(
        self,
        database: pathlib.Path,
        config: configparser.ConfigParser,
        *,
        mode: str = "append",
        only_departement: typing.Union[util.Departement, None] = None
    ):
        """
        :param database: SQLite database to fill.
        :param config: Configuration (see README.md for details).
        :param mode:
            Mode of operation: if "append" current database content is kept and locations are only added for departements
            not represented in the database, if "reset" current database content is erased and locations are added for all
            departements.
        :param only_departement: if not None, only data for this departement will be added.
        """
        
        self.config = config
        self.mode = mode
        self.only_departement = only_departement

        # Initial message
        logging.info(f"Process started. Operation mode: \"{self.mode}\".")
        
        # Create databases
        self.bano = bano.Database(self.config["bano"])
        self.geo_sirene = geo_sirene.Database(self.config["geo-sirene"])
        self.fichiers_personnes_morales = fichiers_personnes_morales.Database(self.config["fichiers-des-locaux"], self.config["fichiers-des-parcelles"])
        self.parcellaire_express = parcellaire_express.Database(self.config["parcellaire-express"])
        self.sirene_unites_legales = sirene_unites_legales.Database(self.config["sirene-unites-legales"])
        
        # Reset database
        if mode == "reset":
            if database.exists():
                logging.info(f"Deleting database file \"{database}\"...")
                database.unlink()
        
        # Connect to the database
        logging.info(f"Connecting to database \"{database}\"...")
        self.connection = sqlite3.connect(database)
        
        # Load Spatialite
        logging.info(f"Loading Spatialite...")
        self.connection.load_extension("mod_spatialite")
        with self.connection:
            if not self.table_exists("spatial_ref_sys"):
                self.connection.execute("select InitSpatialMetaData()")

        # Insert data into the database ; the processes below are separate in several functions to optimize RAM usage
        self.fill_location_address()
        self.fill_entity_big_entity()
        
        # Optimize FTS5 virtual tables
        logging.info(f"Optimizing FTS5 virtual tables...")
        with self.connection:
            self.connection.execute("insert into address(address) values(?)", ("optimize",))
            self.connection.execute("insert into entity(entity) values(?)", ("optimize",))
                        
    def insert(self, table: str, columns: dict[str, typing.Any], *, geometry_columns: list[str] = [], ignore_if_duplicate = False):
        """
        Inserts a row into a table.
        
        :param table: Table name.
        :param columns: Columns to insert.
        :param geometry_columns: Column names to treat as EPSG:4326 geometry columns.
        :param ignore_if_duplicate: Don't generate an error if a row with the same key already exists.
        """
        
        # Prepare placeholders
        placeholders = []
        for name, value in columns.items():
            if name in geometry_columns:
                placeholders.append("GeomFromText(?, 4326)")
            else:
                placeholders.append("?")
                
        # Prepare the request
        request = "insert"
        if ignore_if_duplicate:
            request += " or ignore"
        request += f" into {table}({', '.join(columns.keys())}) values({', '.join(placeholders)})"

        # Execute the request
        self.connection.execute(request, tuple(columns.values()))
        
    def fill_entity_big_entity(self):
        """Fills the "entity" and "big_entity" database tables."""
        
        # Log message
        logging.info(f"Inserting entities...")
        
        with self.connection:
            # Table "entity" stores all entities that appear in table "location", with their name and number
            self.connection.execute("drop table if exists entity");
            self.connection.execute("""
create virtual table entity using fts5(
    name,
    number unindexed,       -- entity number in 9 characters, either the entity SIREN number or something else (matches "entity_number" column in table "location")
    plots unindexed,        -- number of unique plots the entity is related to
    addresses unindexed,    -- number of unique addresses the entity is related to
    tokenize = 'trigram'
)
""")

            # List entities
            entities : dict[str, Entity] = {}
            cursor = self.connection.execute("select type, entity_number, entity_name, departement from location where entity_number not null group by id, entity_number")
            while (location := cursor.fetchone()) != None:
                # Entity is already listed
                if (entity := entities.get(location[1], None)) != None:
                    # Increase the global location count
                    if location[0] == 0:
                        entity.location_count.plots += 1
                    elif location[0] == 1:
                        entity.location_count.addresses += 1
                        
                    # Increase the department-specific location count
                    departement = util.Departement(location[3])
                    if departement in entity.departements:
                        if location[0] == 0:
                            entity.departements[departement].plots += 1
                        elif location[0] == 1:
                            entity.departements[departement].addresses += 1
                    else:
                        entity.departements[departement] = EntityLocationCount(
                            1 if location[0] == 0 else 0,
                            1 if location[0] == 1 else 0
                        )
                # Entity isn't listed yet
                else:
                    # List the entity
                    entities[location[1]] = Entity(
                        location[2],
                        EntityLocationCount(
                            1 if location[0] == 0 else 0,
                            1 if location[0] == 1 else 0,
                        ),
                        {
                            util.Departement(location[3]): EntityLocationCount(
                                1 if location[0] == 0 else 0,
                                1 if location[0] == 1 else 0
                            )
                        }
                    )
                
            # Insert entities into the database
            for number, entity in entities.items():
                self.insert(
                    "entity",
                    {
                        "name": entity.name,
                        "number": number,
                        "plots": entity.location_count.plots,
                        "addresses": entity.location_count.addresses
                    }
                )
                
        # Log message
        logging.info(f"Generating big entities...")
        
        # Bit entities JSON path
        big_entities_json_path = pathlib.Path(os.path.realpath(__file__)).parent / ".." / "www" / "public" / "big_entities.json"
        if not big_entities_json_path.parent.is_dir():
            raise RuntimeError(f"Cannot generate big entities JSON file to {big_entities_json_path} because {big_entities_json_path.parent} isn't a directory.")
            
        # List big entities
        big_entities: dict[str, Entity] = {}
        for number, entity in entities.items():
            if len(entity.departements) > 1 and entity.location_count.plots + entity.location_count.addresses >= self.BIG_ENTITY_MINIMUM_LOCATIONS:
                big_entities[number] = entity
                
        # Generate big entities to a JSON file
        json.dump(
            {
                number: {
                    departement.code: entity_location_count.plots + entity_location_count.addresses
                    for departement, entity_location_count in big_entity.departements.items()
                } for number, big_entity in big_entities.items()
            },
            big_entities_json_path.open("w")
        )
        
    def fill_location_address(self):
        """Fills the "location" and "address" database tables."""
        
        with self.connection:
            # Table "location" lists location-entity relations.
            # Each row must reference either a plot or an address, and can reference one entity.
            # This means data is duplicated to make all entities fit.
            # For example:
            #  - if a plot or address is only related to 0 or 1 entity, it will appear once in the table
            #  - if a plot or address is related to more than 1 entity, it will appear in the table as many times as the number of entities it is related to
            if not self.table_exists("location"):
                self.connection.execute("""
create table location(
    -- common columns used by both plots and addresses
    type integer not null,           -- location type, 0 for plots and 1 for addresses
    id integer not null,             -- location id, all rows that represent the same plot/address have the same value in the "id" column.
    departement text not null,       -- location department as a 2-character or 3-character code
    center_latitude real not null,   -- for plots latitude of the plot center ; for addresses latitude of the address point
    center_longitude real not null,  -- for plots longitude of the plot center ; for addresses longitude of the address point
    -- plot-specific columns
    plot_commune text,               -- first part of the cadastral reference, commune INSEE code in 5 characters
    plot_prefixe text,               -- second part of the cadastral reference, in 3 characters
    plot_section text,               -- third part of the cadastral reference, in 2 characters
    plot_numero text,                -- fourth part of the cadastral reference, in 4 characters
    plot_commune_name text,          -- commune name with mixed-case characters and diacritics
    -- address-specific columns
    address_text text,               -- address text with mixed-case characters and diacritics (matches "original_text" column from "address" table)
    -- optional entity columns
    entity_number text,              -- entity number in 9 characters, either the entity SIREN number or something else (matches "number" column in table "entity")
    entity_relation integer,         -- code representing the relation between the entity and the location (see util.Relation)
    entity_name text,                -- entity name with mixed-case characters and diacritics
    entity_amount integer,           -- amount of this type of relation the entity has with the location (it's more than 1 e.g. for establishments)
    entity_year integer,             -- year in which the relation between the entity and the location was last confirmed
    entity_month integer,            -- month in which the relation between the entity and the location was last confirmed
    entity_details text              -- details about the relation between the entity and the location
                                     --  * when "type" is 0, "entity_relation" is between 0 and 24 included, and the relation between the entity
                                     --    and the plot is about specific premises on the plot and not the whole plot, "entity_details" must be set
                                     --    either to details about the premises in question or to an empty string if no details are available
                                     --  * when "type" is 1 and "entity_relation" is 100 or 101, "entity_details" can be set to details about the
                                     --    headquarters or establishment, or set to null if no details are available
                                     --  * in all other cases, it is null
)
""")
                # Add plot-specific column "plot_geography"
                self.connection.execute("select AddGeometryColumn('location', 'plot_geography', 4326, 'MULTIPOLYGON', 'XY')")
                self.connection.execute("select CreateSpatialIndex('location', 'plot_geography')")
                
                # Add address-specific column "address_geography"
                self.connection.execute("select AddGeometryColumn('location', 'address_geography', 4326, 'POINT', 'XY')")
                self.connection.execute("select CreateSpatialIndex('location', 'address_geography')")
                
                # Add an index to optimize search by address text
                self.connection.execute("create index location_address_text_idx on location(address_text, id)")
                
                # Add indexes to optimize search by entity number
                self.connection.execute("create index location_entity_number_idx on location(entity_number, departement, id)")
                
                # Add an index to optimize search by cadastral reference
                self.connection.execute("create index location_cadastral_reference_idx on location(plot_commune, plot_prefixe, plot_section, plot_numero, id)")
                
            if not self.table_exists("address"):
                # Table "address" stores all addresses
                # It is used to provide search suggestions to the user.
                # The "text" column stores the address text without diacritics and prefixed by a space character.
                #  - This is to allow the sqlite FTS5 trigram tokenizer to consider one-character address numbers as three-character tokens preceded and followed by a space.
                #  - For example, in address " 1 rue du Lac, 31000 Ville", token " 1 " will be registered by the tokenizer.
                # The "original_text" column stores the address text as it appears in the "location" table (with diacritics).
                self.connection.execute("""
create virtual table address using fts5(
    text,
    original_text unindexed,
    tokenize = 'trigram'
)
""")

        # Entity names
        entity_names: dict[str, str] = None
            
        # For each French departement
        for departement in util.ALL_DEPARTEMENTS:
            # If the departement should be processed
            if (self.only_departement == None or departement == self.only_departement) and \
               self.connection.execute("select departement from location where departement = ? limit 1", (departement.code,)).fetchone() == None:
                with self.connection:
                    # Load entity names
                    if entity_names is None:
                        logging.info(f"Loading entity names...")
                        entity_names = self.sirene_unites_legales.all()
                    
                    # Log message
                    logging.info(f"Inserting locations for departement {departement}...")
                    
                    # Load data for the departement
                    addresses: dict[util.AddressKey, adresses.Address] = self.bano.departement(departement)
                    address_entities: dict[util.AddressKey, dict[geo_sirene.EntityRelationKey, geo_sirene.AddressEntity]] = self.geo_sirene.departement(departement)
                    plot_entities: dict[util.PlotKey, dict[fichiers_personnes_morales.EntityRelationKey, fichiers_personnes_morales.PlotEntity]] = self.fichiers_personnes_morales.departement(departement)
                    plots: dict[util.PlotKey, parcellaire_express.Plot] = self.parcellaire_express.departement(departement)
                    
                    # List plot-entity relations from the "fichiers des parcelles des personnes morales" which match plots from "Parcellaire Express"
                    matching_plot_entities: dict[util.PlotKey, dict[fichiers_personnes_morales.EntityRelationKey, fichiers_personnes_morales.PlotEntity]] = {}
                    for plot_key, plot_entities_value in plot_entities.items():
                        if plot_key in plots:
                            matching_plot_entities[plot_key] = plot_entities_value
                    
                    # Log the error rate for plot-entity relations which don't match plots
                    if len(matching_plot_entities) < len(plot_entities):
                        mismatches = len(plot_entities) - len(matching_plot_entities)
                        error_ratio = mismatches / len(plot_entities)
                        logging.warning(f"Unable to match {mismatches}/{len(plot_entities)} plots from the \"fichiers des locaux et parcelles des personnes morales\" to \"Parcellaire Express\" plots for departement \"{departement}\" (error rate: {error_ratio * 100:.4f}%, expected: ~1%).")
                        
                    # List address-entity relations from the Géo-SIRENE database which match addresses from the BANO database
                    matching_address_entities: dict[util.AddressKey, dict[geo_sirene.EntityRelationKey, geo_sirene.AddressEntity]] = {}
                    for address_key, address_entities_value in address_entities.items():
                        if address_key in addresses:
                            matching_address_entities[address_key] = address_entities_value
                    
                    # Log the error rate for address-entity relations which don't match addresses
                    if len(matching_address_entities) < len(address_entities):
                        mismatches = len(address_entities) - len(matching_address_entities)
                        error_ratio = mismatches / len(address_entities)
                        logging.warning(f"Unable to match {mismatches}/{len(address_entities)} addresses from the Géo-SIRENE database to BANO database addresses for departement \"{departement}\" (error rate: {error_ratio * 100:.4f}%, expected ~5%).")
                    
                    # Set the id of the next plot/address that will be inserted in the "location" table
                    if self.connection.execute("select rowid from location limit 1").fetchone() != None:
                        next_id = self.connection.execute("select max(id) from location").fetchone()[0] + 1
                    else:
                        next_id = 1
                    
                    # Insert plots into the "location" table
                    for plot in plots.values():
                        # Set the base columns
                        base_columns: dict[str, typing.Any] = {
                            "type": 0,
                            "id": next_id,
                            "departement": plot.departement.code,
                            "center_latitude": plot.center_latitude,
                            "center_longitude": plot.center_longitude,
                            "plot_commune": plot.key.commune.code,
                            "plot_prefixe": plot.key.prefixe,
                            "plot_section": plot.key.section,
                            "plot_numero": plot.key.numero,
                            "plot_commune_name": plot.commune_name,
                            "plot_geography": plot.geography
                        }
                        
                        # Insert the plot, possibly several times if it is related to several entities
                        if plot_entities_value := matching_plot_entities.get(plot.key, None):
                            for plot_entity in plot_entities_value.values():
                                columns: dict[str, typing.Any] = {
                                    "entity_number": plot_entity.entity_number,
                                    "entity_relation": int(plot_entity.entity_relation),
                                    "entity_name": entity_names.get(plot_entity.entity_number, plot_entity.entity_name), # If available, use the name from "entity_names" as it generally has better formatting
                                    "entity_amount": 1,
                                    "entity_year": plot_entity.entity_year,
                                    "entity_month": plot_entity.entity_month,
                                    "entity_details": plot_entity.entity_details
                                }
                                columns.update(base_columns)
                                self.insert("location", columns, geometry_columns = ["plot_geography"])
                        else:
                            self.insert("location", base_columns, geometry_columns = ["plot_geography"])
                            
                        # Increase the next id
                        next_id += 1
                            
                    # Insert addresses into the "location" table
                    for address in addresses.values():
                        # Set the base columns
                        base_columns: dict[str, typing.Any] = {
                            "type": 1,
                            "id": next_id,
                            "departement": address.departement.code,
                            "center_latitude": address.latitude,
                            "center_longitude": address.longitude,
                            "address_text": address.libelle,
                            "address_geography": address.geography
                        }
                        
                        # Insert the address, possibly several times if it is related to several entities
                        if address_entities_value := matching_address_entities.get(address.key, None):
                            for address_entity in address_entities_value.values():
                                columns: dict[str, typing.Any] = {
                                    "entity_number": address_entity.entity_number,
                                    "entity_relation": int(address_entity.entity_relation),
                                    "entity_name": entity_names.get(address_entity.entity_number, "NOM INCONNU"), # Use the name from "entity_names", or "NOM INCONNU" as a fallback
                                    "entity_amount": address_entity.entity_amount,
                                    "entity_year": address_entity.entity_year,
                                    "entity_month": address_entity.entity_month,
                                    "entity_details": address_entity.entity_details
                                }
                                columns.update(base_columns)
                                self.insert("location", columns, geometry_columns = ["address_geography"])
                        else:
                            self.insert("location", base_columns, geometry_columns = ["address_geography"])
                        
                        # Increase the next id
                        next_id += 1
                            
                    # Insert addresses into the "address" table
                    for address in addresses.values():
                        self.insert(
                            "address",
                            {
                                "text": " " + unidecode.unidecode(address.libelle),
                                "original_text": address.libelle
                            }
                        )
                
    def table_exists(self, table: str) -> bool:
        """Returns True if a given table exists, False otherwise."""
        
        return self.connection.execute(f"select name from sqlite_master where type='table' and name=?", (table,)).fetchone() != None

@dataclasses.dataclass
class EntityLocationCount:
    """Handles the number of unique locations an entity is related to, used when inserting entities into the database "entity" table."""
    
    # Number of unique plots the entity is related to
    plots: int
    # Number of unique addresses the entity is related to
    addresses: int

@dataclasses.dataclass
class Entity:
    """Handles data required to insert an entity into the database "entity" table."""
    
    # Entity name
    name: str
    # Number of unique locations the entity is related to
    location_count: EntityLocationCount
    # Associates departments in which locations the entity is related to are present, associated with the number of such unique locations
    departements: dict[util.Departement, EntityLocationCount]

if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(
        description = "Fills a SQLite database with all French land plots and addresses, including information on entities that own or manage the plots/addresses where available. Geometry of locations saved in the database follows the standard coordinate system EPSG:4326 (WGS 84). You *should* be able to interrupt the script at any time and start it again later without any issue (the department being processed when the script is interrupted will not be inserted), although this has not been thoroughly tested. Note that adding locations for all departments at once is much faster than calling the script repeatedly for each department.",
        epilog = "When running the script, a number of warnings with error rates will most certainly be printed, due to incorrect source data or mismatch between source datasets. Standard, expected error rates will be printed with each warning. As long as error rates stay around their standard values, you're probably fine. To reduce error rates, try to provide datasets that were produced around the same time."
    )
    parser.add_argument("database", type = pathlib.Path, help = "SQLite database to fill")
    parser.add_argument("-c", "--config", default = pathlib.Path(os.path.realpath(__file__)).parent / ".." / "config.ini", help = "configuration file (default: config.ini in the parent directory of this script)")
    parser.add_argument("-v", "--verbose", action = "store_true", help = "if set, informative messages and warnings will be printed (default: unset)")
    parser.add_argument("--mode", type = str, default = "append", choices = ["append", "reset"], help = "mode of operation: if \"append\" current database content is kept and locations are only added for departments not yet processed, if \"reset\" current database content is erased (default: \"append\")")
    parser.add_argument("--department", type = str, default = None, help = "if set, only add locations for the specified department ; the argument should be the INSEE department code, e.g. 03 for Allier, 2A for Corse-du-Sud, 973 for Guyane (default: unset)")
    args = parser.parse_args()
    
    # Set log level
    if args.verbose:
        logging.basicConfig(encoding = "utf-8", level = logging.INFO)
        
    # Read config
    config = configparser.ConfigParser()
    config.read(args.config)
    
    # Only department
    only_department = None
    if args.department != None:
        only_department = util.Departement(args.department)
    
    # Fill database
    Database(
        args.database,
        config,
        mode = args.mode,
        only_departement = only_department
    )
