import argparse
import logging
import pathlib
import shutil

import util

if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(
        description = "Downloads the BANO (Base Adresses Nationale Ouverte) department-specific CSV files to a directory. Does not re-download files that are already present."
    )
    parser.add_argument("directory", type = pathlib.Path, help = "directory where to download the files")
    parser.add_argument("-v", "--verbose", action = "store_true", help = "if set, prints informative messages (default: unset)")
    args = parser.parse_args()
    
    # Set log level
    if args.verbose:
        logging.basicConfig(encoding = "utf-8", level = logging.INFO)
    
    # Download files
    for department in util.ALL_DEPARTEMENTS:
        if department.code not in ["977", "978"]: # These are not covered by the BANO
            filename = f"bano-{department.code}.csv"
            url = f"https://bano.openstreetmap.fr/data/{filename}"
            csv_path = args.directory / filename
            if not csv_path.is_file():
                logging.info(f"Downloading to \"{csv_path}\"...")
                util.download(url, csv_path)
