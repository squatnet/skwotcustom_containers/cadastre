import argparse
import gzip
import logging
import pathlib
import shutil

import util

if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(
        description = "Downloads the Géo-SIRENE latest department-specific files to a directory and extracts them. Does not re-download files that have already been downloaded and/or extracted."
    )
    parser.add_argument("directory", type = pathlib.Path, help = "directory where to download the files")
    parser.add_argument("-v", "--verbose", action = "store_true", help = "if set, prints informative messages (default: unset)")
    args = parser.parse_args()
    
    # Set log level
    if args.verbose:
        logging.basicConfig(encoding = "utf-8", level = logging.INFO)
    
    # Download and extract files
    for department in util.ALL_DEPARTEMENTS:
        # Filenames
        if department.code == "75":
            filenames = [f"geo_siret_75{code}.csv.gz" for code in range(101, 121)]
        else:
            filenames = [f"geo_siret_{department.code}.csv.gz"]
        
        for filename in filenames:
            # Download
            url = f"https://data.cquest.org/geo_sirene/v2019/last/dep/{filename}"
            gzip_path = args.directory / filename
            csv_path = gzip_path.with_suffix("")
            if not gzip_path.is_file() and not csv_path.is_file():
                logging.info(f"Downloading to \"{gzip_path}\"...")
                util.download(url, gzip_path)
                    
            # Extract
            if not csv_path.is_file():
                logging.info(f"Extracting to \"{csv_path}\"...")
                with gzip.open(gzip_path, 'rb') as input_file, open(csv_path, 'wb') as output_file:
                    shutil.copyfileobj(input_file, output_file)
                    
            # Delete archive file
            if gzip_path.is_file():
                gzip_path.unlink()
    
