import argparse
import gzip
import logging
import pathlib
import shutil

import util

if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(
        description = "Downloads the Parcellaire Express latest department-specific files to a directory. Does not re-download files that have already been downloaded."
    )
    parser.add_argument("directory", type = pathlib.Path, help = "directory where to download the files")
    parser.add_argument("date", type = str, help = "date of the files, in the YYYY-MM-DD format")
    parser.add_argument("-v", "--verbose", action = "store_true", help = "if set, prints informative messages (default: unset)")
    args = parser.parse_args()
    
    # Set log level
    if args.verbose:
        logging.basicConfig(encoding = "utf-8", level = logging.INFO)
    
    # Download and extract files
    for department in util.ALL_DEPARTEMENTS:
        # Filename
        if department.code in ["971", "972", "977", "978"]:
            filename = f"PARCELLAIRE_EXPRESS_1-1__SHP_RGAF09UTM20_D{department.code}_{args.date}.7z"
        elif department.code == "976":
            filename = f"PARCELLAIRE_EXPRESS_1-1__SHP_RGM04UTM38S_D976_{args.date}.7z"
        elif department.code == "974":
            filename = f"PARCELLAIRE_EXPRESS_1-1__SHP_RGR92UTM40S_D974_{args.date}.7z"
        elif department.code == "973":
            filename = f"PARCELLAIRE_EXPRESS_1-1__SHP_UTM22RGFG95_D973_{args.date}.7z"
        else:
            filename = f"PARCELLAIRE_EXPRESS_1-1__SHP_LAMB93_D0{department.code}_{args.date}.7z"
        
        # Download
        url = f"https://data.cquest.org/ign/parcellaire-express/{filename}"
        archive_path = args.directory / filename
        if not archive_path.is_file():
            logging.info(f"Downloading to \"{archive_path}\"...")
            util.download(url, archive_path)
