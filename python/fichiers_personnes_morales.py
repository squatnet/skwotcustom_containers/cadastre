from __future__ import annotations

import csv
import enum
import logging
import osgeo.ogr
import pathlib
import re
import typing

import util

class Database:
    """Handles the "fichiers des locaux et parcelles des personnes morales" from the Ministère de l'Économie."""
    
    # These departments are not covered by the "fichiers des locaux et parcelles des personnes morales"
    EXCLUDED_DEPARTMENTS = ["977", "978"]
    
    def __init__(self, config_locaux: configparser.SectionProxy, config_parcelles: configparser.SectionProxy):
        """
        :param config_locaux: Configuration read from the configuration file for the "fichiers des locaux des personnes morales".
        :param config_parcelles: Configuration read from the configuration file for the "fichiers des parcelles des personnes morales".
        """
        
        self.config = {
            Type.LOCAUX: config_locaux,
            Type.PARCELLES: config_parcelles
        }
        
    def departement(self, departement: util.Departement) -> dict[util.PlotKey, dict[EntityRelationKey, PlotEntity]]:
        """Returns all plot-entity relations from a departement."""
        
        # Plot-entity relations
        plot_entities: dict[util.PlotKey, dict[EntityRelationKey, PlotEntity]] = {}
        
        # Number of plot-entity relations that could not be loaded
        unloadable_plot_entities = 0
        
        # Load plot-entity relations
        for fichiers_type in [Type.PARCELLES, Type.LOCAUX]: # "Parcelles" are loaded before "Locaux", this is important
            # Field suffix
            if fichiers_type == Type.LOCAUX:
                field_suffix = "du local"
            elif fichiers_type == Type.PARCELLES:
                field_suffix = "parcelle"
            
            # Iterate rows
            for row in self.rows(departement, fichiers_type):
                # If the entity has a valid number
                if row[f"N° SIREN (Propriétaire(s) {field_suffix})"]:
                    # Identify the departement associated with the row
                    if row["Département (Champ géographique)"] == "97":
                        row_departement = util.Departement(row["Département (Champ géographique)"] + row["Code Direction (Champ géographique)"])
                    else:
                        row_departement = util.Departement(row["Département (Champ géographique)"])
                    if row_departement != departement:
                        raise RuntimeError(f"In the file \"{csv_file_path}\" associated with departement \"{departement}\", a plot is associated with the departement \"{row_departement}\".")
                    
                    # Plot key
                    plot_key = util.PlotKey(
                        util.Commune(row["Département (Champ géographique)"] + row["Code Commune (Champ géographique)"].zfill(3)),
                        row["Préfixe (Références cadastrales)"].replace(' ', '0').zfill(3),
                        row["Section (Références cadastrales)"].zfill(2),
                        row["N° plan (Références cadastrales)"].zfill(4)
                    )
                    
                    # Entity details
                    #  * If the relation between the entity and the plot is about the whole plot, entity_details must be None
                    #  * Otherwise, if the relation is about specific premises on the plot, entity_details must be set, either
                    #    to details about the premises or to an empty string if no details are available
                    entity_details = None
                    entity_details_list = []
                    if fichiers_type == Type.LOCAUX:
                        if (batiment := row['Bâtiment (Identification du local)'].lstrip("0")):
                            if batiment != "A": # Filter "A" because it generally is a default meaningless value
                                entity_details_list.append(f"Bât. {batiment}")
                        if (entree := row["Entrée (Identification du local)"].lstrip("0")):
                            if entree != "1": # Filter "1" because it generally is a default meaningless value
                                entity_details_list.append(f"Entrée {entree}")
                        if row['Niveau (Identification du local)'] and row['Niveau (Identification du local)'].isdigit():
                            niveau = None
                            niveau_int = int(row['Niveau (Identification du local)'])
                            if niveau_int == 0:
                                niveau = "RDC"
                            elif niveau_int >= 81 and niveau_int <= 90: # This generally indicates underground premises
                                niveau = str(80 - niveau_int)
                            elif niveau_int < 70:
                                niveau = row['Niveau (Identification du local)'].lstrip("0")
                            if niveau != None:
                                entity_details_list.append(f"Ét. {niveau}")
                        if entity_details_list:
                            entity_details = " ".join(entity_details_list)
                        else:
                            entity_details = ""
                    
                    # Entity relation key
                    entity_relation_key = EntityRelationKey(
                        row[f"N° SIREN (Propriétaire(s) {field_suffix})"],
                        util.Relation.from_code_droit(row[f"Code droit (Propriétaire(s) {field_suffix})"]),
                        entity_details
                    )
                    
                    # Add the plot-entity relation
                    plot_entities.setdefault(plot_key, {})
                    if entity_relation_key not in plot_entities[plot_key]:
                        plot_entities[plot_key][entity_relation_key] = PlotEntity(
                            plot_key,
                            entity_relation_key.number,
                            entity_relation_key.relation,
                            row[f"Dénomination (Propriétaire(s) {field_suffix})"],
                            self.config[fichiers_type].getint("year"),
                            self.config[fichiers_type].getint("month"),
                            entity_relation_key.details
                        )
                    else:
                        # This entity with this relation with this plot has already been added
                        # This is because of some data duplication in the source dataset, it's normal
                        pass
                else:
                    unloadable_plot_entities += 1
                    
        # Log message
        if plot_entities:
            if unloadable_plot_entities != 0:
                error_ratio = unloadable_plot_entities / (unloadable_plot_entities + len(plot_entities))
                logging.warning(f"Unable to load {unloadable_plot_entities}/{unloadable_plot_entities + len(plot_entities)} plot-entity relations from the \"fichiers des locaux et parcelles des personnes morales\" for departement \"{departement}\" (error rate: {error_ratio * 100:.4f}%, expected: ~0.001%).")

        # Return plot-entity relations
        return plot_entities
        
    def rows(self, departement: util.Departement, fichiers_type: Type):
        """Returns all rows for the given department and type of "fichiers"."""
        
        if departement.code not in self.EXCLUDED_DEPARTMENTS:
            # Regex prefix
            if fichiers_type == Type.LOCAUX:
                regex_prefix = "_B_"
            elif fichiers_type == Type.PARCELLES:
                regex_prefix = "NB_"
            
            # Yield the rows
            for path in pathlib.Path(self.config[fichiers_type]["path"]).rglob("*"):
                if path.suffix == ".txt":
                    # Identify the departement associated with the csv file
                    if (match := re.search(regex_prefix + "([0-9][0-9AB][0-9])", path.name)) != None:
                        if match.group(1).startswith("97"):
                            csv_departement = util.Departement(match.group(1))
                        else:
                            csv_departement = util.Departement(match.group(1)[0:2])
                    else:
                        raise RuntimeError(f"No departement could be associated with the file \"{path}\".")
                    
                    # If the csv file should be processed
                    if csv_departement == departement:
                        with path.open("r", encoding = "ISO-8859-1", newline = "") as csv_file:
                            csv_reader = csv.DictReader(csv_file, delimiter = ";")
                            for row in csv_reader:
                                yield row

class Type(enum.Enum):
    """Type of "fichiers"."""
    
    LOCAUX = 0 # "Fichiers des locaux des personnes morales"
    PARCELLES = 1 # "Fichiers des parcelles des personnes morales"

class EntityRelationKey(typing.NamedTuple):
    """Key that uniquely identifies an entity relation with a plot."""
    
    number: str
    relation: util.Relation
    details: str

class PlotEntity:
    """Relation between a plot and an entity, as described in the "fichiers des parcelles des personnes morales" of the Ministère de l'Économie."""
    
    def __init__(
        self,
        plot_key: util.PlotKey,
        entity_number: str,
        entity_relation: util.Relation,
        entity_name: str,
        entity_year: int,
        entity_month: int,
        entity_details: typing.Union[str, None]
    ):
        """
        :param plot_key: Plot key.
        :param entity_number: Entity number.
        :param entity_relation: Relation between the plot and the entity.
        :param entity_name: Entity name.
        :param entity_year: Year in which the relation between the plot and the entity was last confirmed.
        :param entity_month: Month in which the relation between the plot and the entity was last confirmed.
        :param entity_details: Entity details, used when the relation between the entity and the plot is about specific premises on the plot and not the whole plot.
        """
        
        self.plot_key = plot_key
        self.entity_number = entity_number
        self.entity_relation = entity_relation
        self.entity_name = entity_name
        self.entity_year = entity_year
        self.entity_month = entity_month
        self.entity_details = entity_details
        
    def __str__(self):
        return str((str(self.plot_key), self.entity_number, self.entity_relation, self.entity_name))
