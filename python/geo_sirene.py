from __future__ import annotations

import configparser
import csv
import logging
import osgeo.ogr
import pathlib
import re
import typing

import util

class Database:
    """Handles the Géo-SIRENE database."""
    
    def __init__(self, config: configparser.SectionProxy):
        """
        :param config: Configuration read from the configuration file.
        """
        
        self.config = config
        
    def departement(self, departement: util.Departement) -> dict[util.AddressKey, dict[EntityRelationKey, AddressEntity]]:
        """Returns all address-entity relations from a departement."""
        
        # Address-entity relations
        address_entities: dict[util.AddressKey, dict[EntityRelationKey, AddressEntity]] = {}
        
        # Load address-entity relations
        for row in self.rows(departement):
            # Establishment is active and was geolocalised to a specific number
            if row["etatAdministratifEtablissement"] == "A" and row["geo_type"] == "housenumber":
                # Entity relation
                if row["etablissementSiege"] == "true":
                    entity_relation = util.Relation.SIEGE_SOCIAL
                else:
                    entity_relation = util.Relation.ETABLISSEMENT
                    
                # Entity details
                if row["denominationUsuelleEtablissement"] and row["denominationUsuelleEtablissement"] != "[ND]":
                    entity_details = row["denominationUsuelleEtablissement"]
                else:
                    entity_details = None
                
                # Add the address-entity relation
                address_key = util.AddressKey(row["geo_l4"], util.Commune(row["codeCommuneEtablissement"]))
                entity_relation_key = EntityRelationKey(row["siren"], entity_relation, entity_details)
                address_entities.setdefault(address_key, {})
                if entity_relation_key not in address_entities[address_key]:
                    address_entities[address_key][entity_relation_key] = AddressEntity(
                        address_key,
                        entity_relation_key.number,
                        entity_relation_key.relation,
                        self.config.getint("year"),
                        self.config.getint("month"),
                        entity_relation_key.details
                    )
                else:
                    address_entities[address_key][entity_relation_key].entity_amount += 1
                    
        # Return address-entity relations
        return address_entities
        
    def rows(self, department: util.Departement):
        """Generator function which yields all rows for a given department."""
        
        csv_paths: list[pathlib.Path] = []
        for path in pathlib.Path(self.config["path"]).rglob("*"):
            if path.name.startswith(f"geo_siret_{department.code}") and path.suffix == ".csv" and path.is_file():
                csv_paths.append(path)
        if csv_paths:
            for path in csv_paths:
                with path.open("r", newline = "") as csv_file:
                    csv_reader = csv.DictReader(csv_file, delimiter = ",")
                    for row in csv_reader:
                        yield row
        else:
            raise RuntimeError(f"No files could be found in the Géo-SIRENE database for department \"{department}\".")

class EntityRelationKey(typing.NamedTuple):
    """Key that uniquely identifies an entity relation with an address."""
    
    number: str
    relation: util.Relation
    details: str

class AddressEntity:
    """Relation between an address and an entity, as described in the "Établissements" SIRENE database."""
    
    def __init__(
        self,
        address_key: util.AddressKey,
        entity_number: str,
        entity_relation: util.Relation,
        entity_year: int,
        entity_month: int,
        entity_details: typing.Union[str, None]
    ):
        """
        :param address_key: Address key.
        :param entity_number: Entity SIREN number.
        :param entity_relation: Relation between the address and the entity.
        :param entity_year: Year in which the relation between the address and the entity was last confirmed.
        :param entity_month: Month in which the relation between the address and the entity was last confirmed.
        :param entity_details: Entity details, used when details are available about the headquarters or establishment covered by the relation.
        """
        
        self.address_key = address_key
        self.entity_number = entity_number
        self.entity_relation = entity_relation
        self.entity_year = entity_year
        self.entity_month = entity_month
        self.entity_details = entity_details
        
        # Amount of this type of relation the entity has with the address (e.g. number of establishments the entity has at this address)
        self.entity_amount = 1
