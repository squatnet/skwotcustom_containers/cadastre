from __future__ import annotations

import logging
import osgeo.ogr
import pathlib
import re
import typing

import util

class Database:
    """Handles IGN "Parcellaire Express" database."""
    
    def __init__(self, config: configparser.SectionProxy):
        """
        :param config: Configuration read from the configuration file.
        """
        
        self.config = config
        
    def departement(self, departement: util.Departement) -> dict[util.PlotKey, Plot]:
        """Returns all plots from a departement."""
        
        # Plots
        plots: dict[util.PlotKey, Plot] = {}
        
        # Number of plots that could not be loaded
        unloadable_plots = 0

        # Find the SHP file that matches the departement
        shp_path = None
        for path in pathlib.Path(self.config["path"]).rglob("*"):
            if path.name == "PARCELLE.SHP":
                if (match := re.search(r"D([0|9][0-9][0-9AB])", path.parent.name)) != None:
                    if match.group(1)[0] == "0":
                        shp_departement = util.Departement(match.group(1)[1:3])
                    else:
                        shp_departement = util.Departement(match.group(1))
                else:
                    raise RuntimeError(f"No departement could be associated with the file \"{path}\".")
                    
                if shp_departement == departement:
                    shp_path = path
                    break
                    
        # Raise an exception if no SHP file was found
        if shp_path == None:
            raise RuntimeError(f"No SHP file was found for departement \"{departement}\" in database \"{self.config['path']}\".")

        # Open the SHP file
        shapefile_driver = osgeo.ogr.GetDriverByName("ESRI Shapefile")
        input_data_source = shapefile_driver.Open(str(shp_path))
        input_layer = input_data_source.GetLayer()
        
        # Create the appropriate coordinate transformation
        input_spatial_reference = input_layer.GetSpatialRef()
        output_spatial_reference = osgeo.osr.SpatialReference()
        output_spatial_reference.ImportFromEPSG(4326)
        coordinate_transformation = osgeo.osr.CoordinateTransformation(input_spatial_reference, output_spatial_reference)
        
        # Load plots
        for feature in input_layer:
            # Identify the departement
            if feature.GetField("CODE_DEP") == "97":
                feature_departement = util.Departement(feature.GetField("CODE_DEP") + feature.GetField("CODE_COM")[0])
            else:
                feature_departement = util.Departement(feature.GetField("CODE_DEP"))
            if feature_departement != departement:
                raise RuntimeError(f"In the file \"{shp_path}\" associated with departement \"{departement}\", a plot is associated with departement \"{feature_departement}\".")
            
            # Identify the commune
            commune = None
            if feature.GetField("CODE_ARR") != "000" and feature.GetField("CODE_ARR") != None:
                commune = util.Commune(departement.code[0:2] + feature.GetField("CODE_ARR"))
            elif feature.GetField("CODE_COM") != None:
                commune = util.Commune(departement.code[0:2] + feature.GetField("CODE_COM"))
            
            # If a commune has been identified
            if commune:
                # Get the feature geometry
                feature_geometry = feature.GetGeometryRef()
                
                # If the feature has a geometry
                if feature_geometry:
                    # Transform the geometry
                    feature_geometry.Transform(coordinate_transformation)
                    
                    # Add the plot
                    plot_key = util.PlotKey(
                        commune,
                        feature.GetField("COM_ABS"),
                        feature.GetField("SECTION"),
                        feature.GetField("NUMERO")
                    )
                    if plot_key not in plots:
                        plots[plot_key] = Plot(plot_key, departement, feature.GetField("NOM_COM"), feature_geometry)
                    else:
                        unloadable_plots += 1
                else:
                    unloadable_plots += 1
            else:
                unloadable_plots += 1
                
        # Log message
        if plots:
            if unloadable_plots != 0:
                error_ratio = unloadable_plots / (unloadable_plots + len(plots))
                logging.warning(f"Unable to load {unloadable_plots}/{unloadable_plots + len(plots)} plots from \"Parcellaire Express\" database for departement \"{departement}\" (error rate: {error_ratio * 100:.4f}%, expected: ~0.001%).")
            
        # Return plots
        return plots

class Plot:
    """Plot from IGN "Parcellaire Express" database."""
    
    def __init__(self, key: util.PlotKey, departement: util.Departement, commune_name: str, geometry: osgeo.ogr.Geometry):
        """
        :param key: Plot key.
        :param departement: Plot departement.
        :param commune_name: Commune name, with lower-case and upper-case characters and accents.
        :param geometry: Plot geometry (can be either a polygon or a multi-polygon).
        """
        
        self.key = key
        self.departement = departement
        self.commune_name = commune_name
        
        # Set geography
        if geometry.GetGeometryName() == "POLYGON":
            geometry = osgeo.ogr.ForceToMultiPolygon(geometry)
        self.geography = str(geometry)
        
        # Set center
        center = geometry.Centroid()
        self.center_latitude = center.GetX()
        self.center_longitude = center.GetY()
