from __future__ import annotations

import csv
import logging
import osgeo.ogr
import pathlib
import re
import typing

import util

class Database:
    """Handles the "Établissements" SIRENE database."""
    
    # Dict that links types of way found in the "Établissements" SIRENE database with normalized types of way listed in util.AddressKey.TYPES_OF_WAY
    TYPES_OF_WAY = {
        "AIRE": "aire",
        "ALL": "allée",
        "AV": "avenue",
        "BASE": "base",
        "BD": "boulevard",
        "CAMI": "cami",
        "CAR": "carrefour",
        "CHE": "chemin",
        "CHEM": "cheminement",
        "CHS": "chaussée",
        "CITE": "cité",
        "CLOS": "clos",
        "COIN": "coin",
        "COR": "corniche",
        "COTE": "côte",
        "COUR": "cour",
        "CRS": "cours",
        "DOM": "domaine",
        "DSC": "descente",
        "ECA": "écart",
        "ESP": "esplanade",
        "FG": "faubourg",
        "GARE": "gare",
        "GR": "grande rue",
        "HAM": "hameau",
        "HLE": "halle",
        "ILOT": "ilôt",
        "IMP": "impasse",
        "LD": "lieu-dit",
        "LOT": "lotissement",
        "MAR": "marché",
        "MTE": "montée",
        "PARC": "parc",
        "PAS": "passage",
        "PL": "place",
        "PLAN": "plan",
        "PLN": "plaine",
        "PLT": "plateau",
        "PONT": "pont",
        "PORT": "port",
        "PRO": "promenade",
        "PRV": "parvis",
        "QUA": "quartier",
        "QUAI": "quai",
        "RES": "résidence",
        "RLE": "ruelle",
        "ROC": "rocade",
        "RPT": "rond-point",
        "RTE": "route",
        "RUE": "rue",
        "SEN": "sentier",
        "SQ": "square",
        "TOUR": "tour",
        "TPL": "terre-plein",
        "TRA": "traverse",
        "VLA": "villa",
        "VLGE": "village",
        "VOIE": "voie",
        "ZA": "zone artisanale",
        "ZAC": "zone d'aménagement concerté",
        "ZAD": "zone d'aménagement différé",
        "ZI": "zone industrielle",
        "ZONE": "zone"
    }
    
    def __init__(self, directory: pathlib.Path):
        """
        :param directory: Directory containing the files.
        """
        
        self.directory = directory
        
    def departement(self, departement: util.Departement) -> dict[util.AddressKey, dict[EntitySirenRelation, AddressEntity]]:
        """Returns all address-entity relations from a departement."""
        
        # Address-entity relations
        address_entities: dict[util.AddressKey, dict[EntitySirenRelation, AddressEntity]] = {}
        
        # Number of address-entity relations that could not be loaded
        unloadable_address_entities = 0
        
        # Load address-entity relations
        for row in self.rows():
            # Address is in the right departement and establishment is active
            if row["codeCommuneEtablissement"].startswith(departement.code) and row["etatAdministratifEtablissement"] == "A":
                try:
                    # Address number
                    number = None
                    if row["numeroVoieEtablissement"] and row["numeroVoieEtablissement"] != "[ND]":
                        if row["numeroVoieEtablissement"].isdigit():
                            numero = int(row["numeroVoieEtablissement"])
                            if row["indiceRepetitionEtablissement"] and row["indiceRepetitionEtablissement"] != "[ND]":
                                number = util.AddressNumber(numero, row["indiceRepetitionEtablissement"].strip())
                            else:
                                number = util.AddressNumber(numero, None)
                        else:
                            raise BadSourceData()
                            
                    # Address way
                    if row["libelleVoieEtablissement"] and row["libelleVoieEtablissement"] != "[ND]":
                        if row["typeVoieEtablissement"] and row["typeVoieEtablissement"] != "[ND]":
                            if row["typeVoieEtablissement"] in self.TYPES_OF_WAY:
                                way = util.AddressWay(
                                    self.TYPES_OF_WAY[row["typeVoieEtablissement"]],
                                    row["libelleVoieEtablissement"]
                                )
                            else:
                                raise BadSourceData()
                        else:
                            way = util.AddressWay(None, row["libelleVoieEtablissement"])
                            
                    # Address commune
                    commune = util.Commune(row["codeCommuneEtablissement"])
                    
                    # Entity relation
                    if row["etablissementSiege"] == "true":
                        entity_relation = util.Relation.SIEGE_SOCIAL
                    else:
                        entity_relation = util.Relation.ETABLISSEMENT
                    
                    # Add the address-entity relation
                    address_key = util.AddressKey(number, way, None, commune)
                    entity_siren_relation = EntitySirenRelation(row["siren"], entity_relation)
                    address_entities.setdefault(address_key, {})
                    if entity_siren_relation not in address_entities[address_key]:
                        address_entities[address_key][entity_siren_relation] = AddressEntity(
                            address_key,
                            row["siren"],
                            entity_relation
                        )
                    else:
                        address_entities[address_key][entity_siren_relation].entity_amount += 1
                # Bad source data
                except BadSourceData:
                    unloadable_address_entities += 1
                    
        # Log message
        if unloadable_address_entities:
            if unloadable_address_entities != 0:
                error_ratio = unloadable_address_entities / (unloadable_address_entities + len(address_entities))
                logging.warning(f"Unable to load {unloadable_address_entities}/{unloadable_address_entities + len(address_entities)} address-entity relations from the \"Établissements\" SIRENE database (error rate: {error_ratio * 100:.4f}%, expected ~1%).")
        else:
            raise RuntimeError(f"Unable to load any address-entity relations for departement \"{departement}\" from the \"Établissements\" SIRENE database.")
            
        # Return address-entity relations
        return address_entities
        
    def rows(self):
        """Generator function which yields all rows from the database."""
        
        csv_path: typing.Union[pathlib.Path, None] = None
        for path in self.directory.rglob("*"):
            if path.name == "StockEtablissement_utf8.csv" and path.is_file():
                csv_path = path
                break
        if csv_path == None:
            raise RuntimeError(f"The necessary file could not be found in the \"adresses\" database in \"{self.directory}\".")
        with path.open("r", newline = "") as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter = ",")
            for row in csv_reader:
                yield row

class EntitySirenRelation(typing.NamedTuple):
    """References an entity SIREN number and relation."""
    
    siren: str
    relation: util.Relation

class AddressEntity:
    """Relation between an address and an entity, as described in the "Établissements" SIRENE database."""
    
    def __init__(self, address_key: util.AddressKey, entity_siren: str, entity_relation: util.Relation):
        """
        :param address_key: Address key.
        :param entity_siren: Entity SIREN number.
        :param entity_relation: Relation between the address and the entity.
        """
        
        self.address_key = address_key
        self.entity_siren = entity_siren
        self.entity_relation = entity_relation
        
        # Amount of this type of relation the entity has with the address (e.g. number of establishments the entity has at this address)
        self.entity_amount = 1
        
class BadSourceData(Exception):
    """Exception used to signal bad source data."""
    
    pass
