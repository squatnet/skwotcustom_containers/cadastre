from __future__ import annotations

import csv
import logging
import osgeo.ogr
import pathlib
import re
import typing

import util

class Database:
    """Handles the "Unités légales" SIRENE database."""
    
    def __init__(self, config: configparser.SectionProxy):
        """
        :param config: Configuration read from the configuration file.
        """
        
        self.config = config
        
    def all(self) -> dict[str, str]:
        """Returns all entity names."""
        
        # Entities
        entity_names: dict[str, str] = {}
    
        # Number of entities that could not be loaded
        unloadable_entities = 0
        
        # Load entities
        for row in self.rows():
            # Entity hasn't closed before 2002
            if row["unitePurgeeUniteLegale"] != "true":
                # Entity name
                entity_name = None
                if (denomination := self.sanitize_name(row["denominationUniteLegale"])) != "":
                    entity_name = denomination
                elif (nom := self.sanitize_name(row["nomUniteLegale"])) != "":
                    entity_name = nom
                    if (prenom := self.sanitize_name(row[f"prenom1UniteLegale"])) != "":
                        entity_name += f" {prenom}"
                    
                # An entity name was found
                if entity_name != None:
                    # Add the entity
                    if row["siren"] not in entity_names:
                        entity_names[row["siren"]] = entity_name
                    else:
                        raise RuntimeError(f"Several entities with SIREN number \"{row['siren']}\" are present in the \"Unités légales\" SIRENE database.")
                else:
                    unloadable_entities += 1
                    
        # Log message
        if entity_names:
            if unloadable_entities != 0:
                error_ratio = unloadable_entities / (unloadable_entities + len(entity_names))
                logging.warning(f"Unable to load {unloadable_entities}/{unloadable_entities + len(entity_names)} entities from the \"Unités légales\" SIRENE database (error rate: {error_ratio * 100:.4f}%, expected ~0.01%).")
            
        # Return entity names
        return entity_names
        
    def rows(self):
        """Generator function which yields all rows from the database."""
        
        csv_path: typing.Union[pathlib.Path, None] = None
        for path in pathlib.Path(self.config["path"]).rglob("*"):
            if path.name == "StockUniteLegale_utf8.csv" and path.is_file():
                csv_path = path
                break
        if csv_path == None:
            raise RuntimeError(f"Required file not found in the \"Unités légales\" SIRENE database.")
        with path.open("r", newline = "") as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter = ",")
            for row in csv_reader:
                yield row
                
    def sanitize_name(self, name: str) -> str:
        """Sanitizes a name by removing leading and trailing spaces, as well changing consecutive spaces into one space inside the string."""
        
        return re.sub(" +", " ", name.strip())
