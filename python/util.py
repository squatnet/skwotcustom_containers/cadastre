from __future__ import annotations

import enum
import re
import shutil
import unidecode
import urllib3
import tempfile
import time
import typing

class AddressKey:
    """Common key used to identify an address across data sources."""
    
    # Regexes used to correct "libellés"
    SPECIAL_CHARACTERS_REGEX = re.compile("['-]")
    PREPOSITION_REGEX = re.compile("(^| )((DE|DU|DES|LE|LA|D)($| ))+")
    
    # Abbreviations used to correct "libellés"
    ABBREVIATIONS = {
        "DR": "DOCTEUR",
        "PR": "PROFESSEUR",
        "ST": "SAINT"
    }
    
    # Abbrevitations used to correct "libellé", but only if they appear after a digit (to avoid false positives)
    ABBREVIATIONS_AFTER_DIGITS = {
        "ALL": "ALLEE",
        "AV": "AVENUE",
        "CHE": "CHEMIN",
        "CHEM": "CHEMIN",
        "LOT": "LOTISSEMENT",
        "RTE": "ROUTE"
    }
    
    def __init__(self, libelle: str, commune: Commune):
        """
        :param libelle:
            Address "libellé", e.g. "123 Rue du Lac".
            Character case does not matter as it will be uniformized.
            Diacritics and prepositions ("de", "du"...) do not matter as they will be removed.
            Special characters "'" and "-" will be replaced by spaces.
            Some abbreviations (see ABBREVIATIONS) will be expanded.
        :param commune: Address commune.
        """
        
        # Some corrections on the "libellé"
        self.libelle = unidecode.unidecode(libelle) # Remove diacritics
        self.libelle = self.SPECIAL_CHARACTERS_REGEX.sub(" ", self.libelle) # Replace special characters with spaces
        self.libelle = self.libelle.upper() # Uniformize case
        self.libelle = self.PREPOSITION_REGEX.sub(" ", self.libelle) # Remove prepositions
        
        # Replace abbreviations from the "libellé"
        for abbreviation, expansion in self.ABBREVIATIONS.items():
            self.libelle = re.sub(f"(^| ){abbreviation}($| )", f" {expansion} ", self.libelle)
        for abbreviation, expansion in self.ABBREVIATIONS_AFTER_DIGITS.items():
            self.libelle = re.sub(f"([0-9]) {abbreviation}($| )", f"\\1 {expansion} ", self.libelle)
            
        # Remove consecutive spaces from the "libellé"
        self.libelle = re.sub(" +", " ", self.libelle)
        
        # Commune
        self.commune = commune
        
    def __eq__(self, other):
        if isinstance(other, AddressKey):
            return self.libelle == other.libelle and \
                   self.commune == other.commune
        return False
        
    def __hash__(self):
        return hash((self.libelle, self.commune))
        
    def __str__(self):
        return str((self.libelle, str(self.commune)))

class Commune:
    """French commune."""
    
    def __init__(self, code: str):
        """
        :param code: Commune INSEE code in five characters.
        """
        
        if len(code) != 5:
            raise RuntimeError(f"Invalid commune INSEE code \"{code}\".")
        
        self.code = code
        
    def __eq__(self, other):
        if isinstance(other, Commune):
            return self.code == other.code
        return False
        
    def __hash__(self):
        return hash(self.code)
        
    def __str__(self):
        return self.code

class Departement:
    """French departement."""
    
    VALID_CODES = [
        "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
        "2A", "2B", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37",
        "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56",
        "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75",
        "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94",
        "95", "971", "972", "973", "974", "976", "977", "978"
    ]
    
    def __init__(self, code: str):
        """
        :param code: Departement INSEE code in two or three characters, e.g. 03 for Allier, 15 for Cantal, 2A for Corse-du-Sud, 973 for Guyane.
        """
        
        if code not in self.VALID_CODES:
            raise RuntimeError(f"Invalid departement INSEE code \"{code}\".")
        
        self.code = code
        
    def __eq__(self, other):
        if isinstance(other, Departement):
            return self.code == other.code
        return False
        
    def __hash__(self):
        return hash(self.code)
        
    def __str__(self):
        return self.code

ALL_DEPARTEMENTS = [Departement(code) for code in Departement.VALID_CODES]

class PlotKey(typing.NamedTuple):
    """Common key used to identify a plot across data sources."""
    
    commune: Commune
    prefixe: str
    section: str
    numero: str
    
    def __str__(self):
        return str((str(self.commune), self.prefixe, self.section, self.numero))

class Relation(enum.IntEnum):
    """Relation between an entity (legal entity or private individual) and a plot."""
    
    # Relations corresponding to "codes droit" used by the DGFiP in the "fichiers des parcelles des personnes morales"
    CODE_DROIT_P = 0
    CODE_DROIT_U = 1
    CODE_DROIT_N = 2
    CODE_DROIT_B = 3
    CODE_DROIT_R = 4
    CODE_DROIT_F = 5
    CODE_DROIT_T = 6
    CODE_DROIT_D = 7
    CODE_DROIT_V = 8
    CODE_DROIT_W = 9
    CODE_DROIT_A = 10
    CODE_DROIT_E = 11
    CODE_DROIT_K = 12
    CODE_DROIT_L = 13
    CODE_DROIT_G = 14
    CODE_DROIT_S = 15
    CODE_DROIT_H = 16
    CODE_DROIT_O = 17
    CODE_DROIT_J = 18
    CODE_DROIT_Q = 19
    CODE_DROIT_X = 20
    CODE_DROIT_Y = 21
    CODE_DROIT_C = 22
    CODE_DROIT_M = 23
    CODE_DROIT_Z = 24
    # Relations corresponding to the state of entities establishments in the Géo-SIRENE database
    SIEGE_SOCIAL = 100
    ETABLISSEMENT = 101
    
    @classmethod
    def from_code_droit(cls, code_droit: str):
        """Returns the relation corresponding to a "code droit" used by the DGFiP in the "fichiers des parcelles des personnes morales"."""
        
        if code_droit == "P":
            return cls.CODE_DROIT_P
        if code_droit == "U":
            return cls.CODE_DROIT_U
        if code_droit == "N":
            return cls.CODE_DROIT_N
        if code_droit == "B":
            return cls.CODE_DROIT_B
        if code_droit == "R":
            return cls.CODE_DROIT_R
        if code_droit == "F":
            return cls.CODE_DROIT_F
        if code_droit == "T":
            return cls.CODE_DROIT_T
        if code_droit == "D":
            return cls.CODE_DROIT_D
        if code_droit == "V":
            return cls.CODE_DROIT_V
        if code_droit == "W":
            return cls.CODE_DROIT_W
        if code_droit == "A":
            return cls.CODE_DROIT_A
        if code_droit == "E":
            return cls.CODE_DROIT_E
        if code_droit == "K":
            return cls.CODE_DROIT_K
        if code_droit == "L":
            return cls.CODE_DROIT_L
        if code_droit == "G":
            return cls.CODE_DROIT_G
        if code_droit == "S":
            return cls.CODE_DROIT_S
        if code_droit == "H":
            return cls.CODE_DROIT_H
        if code_droit == "O":
            return cls.CODE_DROIT_O
        if code_droit == "J":
            return cls.CODE_DROIT_J
        if code_droit == "Q":
            return cls.CODE_DROIT_Q
        if code_droit == "X":
            return cls.CODE_DROIT_X
        if code_droit == "Y":
            return cls.CODE_DROIT_Y
        if code_droit == "C":
            return cls.CODE_DROIT_C
        if code_droit == "M":
            return cls.CODE_DROIT_M
        if code_droit == "Z":
            return cls.CODE_DROIT_Z
        else:
            raise RuntimeError(f"Invalid \"code droit\" \"{code_droit}\".")

def download(url: str, path: pathlib.Path, *, max_tries = 3, seconds_between_tries = 5) -> None:
    """Downloads a file from a URL to a path. If the download fails, no file is written to the path and an exception is raised."""
    
    tries = 0
    while tries < max_tries:
        tries += 1
        if tries != 1:
            time.sleep(seconds_between_tries)
        try:
            http = urllib3.PoolManager()
            with http.request("GET", url, preload_content = False) as response:
                response.enforce_content_length = True # with this, a urllib3.exceptions.ProtocolError is thrown in case of an incomplete download
                if response.status == 200:
                    with tempfile.NamedTemporaryFile("wb") as temporary_file:
                        shutil.copyfileobj(response, temporary_file) # if this finishes without an exception being thrown, it means the download succeeded
                        shutil.copy(temporary_file.name, path)
                        return
                else:
                    raise RuntimeError(f"HTTP request for {url} returned {response.status} status code.")
        except urllib3.exceptions.ProtocolError as protocol_error:
            pass
    raise RuntimeError(f"Download failed.")
