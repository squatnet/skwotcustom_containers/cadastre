L.Control.LocationSearch = L.Control.extend({
    // called when a chunk of markers has been added
    chunkProgress: function(processed, total, elapsed, layersArray){
        if(processed === total){
            // search is not ongoing anymore
            this.currentSearch.ongoing = false;
            this.setCurrentSearchStatus(SearchingStatus.markersLoaded);
        }
        else{
            // set searching status
            this.setCurrentSearchStatus(SearchingStatus.loadingMarkers, processed / total);
        }
    },
    
    // creates a marker with at a given position with a given icon
    createMarker : function(position, icon, type, id){
        const marker = L.marker(
            position,
            {
                "icon": icon,
                "id": id, // id is here as a custom option so we can open the corresponding location layer's tooltip when clicking on the marker
                "type": type // type is here as a custom option so we can later customize clusters according to their markers types
            }
        );
        marker.on("click", (event) => {
            // if the corresponding location is displayed on the map, simulate a click on it
            let locationDisplayed = false;
            for(const locationType of [0, 1]){
                for(const layer of squatMap.locationLayerGroups[locationType].getLayers()){
                    if(event.target.options.id === layer.feature.properties.id){
                        squatMap.createLocationLayerPopup(layer, event.latlng);
                        layer.openPopup();
                        locationDisplayed = true;
                    }
                }
            }
            
            // if the location isn't displayed on the map, fly to it
            if(locationDisplayed === false){
                squatMap.map.flyTo(event.latlng, 19, {
                    "duration": 1.5
                });
            }
        });
        return marker;
    },
    
    // creates a new search object based on the current values of the input fields, if one can be created (otherwise return null)
    createSearch : function(){
        const trimmedSearchString = this.inputField.value.trim();
        const noWhitespaceSearchString = trimmedSearchString.replace(/\s/g, "");
        // entity
        if(this.entityRegex.test(noWhitespaceSearchString) === true){
            let bigEntity = null;
            if(this.bigEntities !== null && Object.hasOwn(this.bigEntities, noWhitespaceSearchString)){
                bigEntity = this.bigEntities[noWhitespaceSearchString];
            }
            
            return {
                "bigEntity": bigEntity,
                "departement": this.inputSelectField.style.display === "block" && this.inputSelectField.value !== ""? this.inputSelectField.value : null,
                "type": SearchType.entity,
                "string": noWhitespaceSearchString
            };
        }
        // cadastral reference
        else if(this.cadastralReferenceRegex.test(noWhitespaceSearchString) === true){
            return {
                "bigEntity": null,
                "departement": null,
                "type": SearchType.cadastralReference,
                "string": noWhitespaceSearchString
            };
        }
        // address
        else if(noWhitespaceSearchString.length > 0){
            return {
                "bigEntity": null,
                "departement": null,
                "type": SearchType.address,
                "string": trimmedSearchString
            };
        }
        
        // invalid search
        return null;
    },
    
    // flies the map to the current search bounds
    flyToCurrentSearchBounds : function(){
        if(this.currentSearch.bounds !== null){
            squatMap.map.flyToBounds(this.currentSearch.bounds, {
                "duration": 1.5,
                "maxZoom": 19,
                "padding": [50, 50]
            });
        }
    },
     
    // called when the control is added
    onAdd: function(map){
        // load big entities
        this.bigEntities = null;
        fetch('/big_entities.json').then(response => {
            if(!response.ok){
                throw new Error("HTTP error " + response.status);
            }
            return response.json();
        }).then(json => {
            this.bigEntities = json;
        }).catch((error) => {
        });
        
        // current search, which parameters are used to color plots that appear on the map
        this.currentSearch = {
            "addressCount": 0, // number of addresses found in the search
            "bigEntity": null, // big entity matching the search
            "bounds": null,
            "departement": null,
            "ongoing": false, // true if the search is ongoing ; a search is considered ongoing until it fails or until all markers that should be added as a result of the search have been added
            "plotCount": 0, // number of plots found in the search
            "status": SearchingStatus.noStatus,
            "string": null,
            "type": null // uses the SearchType enum
        };
        
        // true if a request for suggestions is ongoing
        this.suggesting = false;
        
        // true if a request for suggestions should be made when the current one is over
        this.chainSuggestionRequest = false;
        
        // if of the next request for suggestion
        this.nextSuggestionRequestId = 0;
        
        // regexes
        this.entityRegex = new RegExp("^[0-9U][0-9]{8}$");
        this.cadastralReferenceRegex = new RegExp("^[0-9]{8}[0-9A-Z]{2}[0-9]{4}$");
        
        // maximum allowed time for requests, in milliseconds
        this.searchRequestTimeout = 30000;
        this.suggestionsRequestTimeout = 15000;
        
        // container
        const container = L.DomUtil.create("div");
        container.id = "location-search";
        L.DomEvent.on(container, "keydown", (event) => {
            // prevent arrow keys from scrolling the browser scroll bar directly
            if(["ArrowUp","ArrowDown"].indexOf(event.code) > -1){
                event.preventDefault();
            }
            
            // allow using arrow keys to scroll down the list of suggestions
            if(event.code === "ArrowUp" || event.code === "ArrowDown"){
                const suggestions = document.getElementsByClassName("location-search-suggestions-content-suggestion");
                if(suggestions.length > 0){
                    if(document.activeElement.classList.contains("location-search-suggestions-content-suggestion")){
                        for(let i = 0; i < suggestions.length; i++){
                            if(document.activeElement === suggestions[i]){
                                if(event.code === "ArrowUp"){
                                    if(i !== 0){
                                        suggestions[i - 1].focus();
                                    }
                                    else{
                                        this.inputField.focus();
                                        document.getElementById("location-search-suggestions").scroll({"top": 0});
                                    }
                                    break;
                                }
                                else if(event.code === "ArrowDown" && i !== suggestions.length - 1){
                                    suggestions[i + 1].focus();
                                    break;
                                }
                            }
                        }
                    }
                    else if(document.activeElement === this.inputField ||
                            document.activeElement === this.inputSelectField ||
                            document.activeElement === this.inputButton){
                        if(event.code === "ArrowDown"){
                            suggestions[0].focus();
                        }  
                    }
                }
            }
        });
        
        // main column
        const mainColumn = L.DomUtil.create("div", null, container);
        mainColumn.id = "location-search-main-column";
        L.DomEvent.disableClickPropagation(mainColumn);
        
        // input
        const input = L.DomUtil.create("div", null, mainColumn);
        input.id = "location-search-input";
        
        // input field
        this.inputField = L.DomUtil.create("input", null, input);
        this.inputField.id = "location-search-input-field";
        this.inputField.type = "search";
        this.inputField.placeholder = "Adresse, entreprise, référence cadastrale...";
        L.DomEvent.on(this.inputField, "keyup", ({key}) => {
            if(key === "Enter"){
                this.search();
            }
        });
        L.DomEvent.on(this.inputField, "input", () => {
            this.updateInputSelectField();
            this.updateSuggestions();
        });
        
        // input select field (used to specify a departement when searching big entities)
        const inputSelectContainer = L.DomUtil.create("div", null, input);
        inputSelectContainer.id = "location-search-input-select-container";
        this.inputSelectField = L.DomUtil.create("select", null, inputSelectContainer);
        this.inputSelectField.id = "location-search-input-select-field";
        L.DomEvent.on(this.inputSelectField, "input", () => {
            if(this.inputSelectField.value !== ""){
                this.search();
            }
        });
        this.updateInputSelectField();
        
        // input button
        const inputButton = L.DomUtil.create("button", null, input);
        inputButton.id = "location-search-input-button";
        L.DomEvent.on(inputButton, "click", () => {
            this.search();
        });
        
        // suggestions
        this.suggestions = L.DomUtil.create("div", null, mainColumn);
        this.suggestions.id = "location-search-suggestions";
        L.DomEvent.on(
            this.suggestions,
            "wheel",
            // prevent wheel events to propagate to the map below
            (event) => { event.stopPropagation(); }
        );
        
        // entity suggestions
        this.entitySuggestions = L.DomUtil.create("div", null, this.suggestions);
        this.entitySuggestions.id = "location-search-suggestions-entities";
        const entitySuggestionsTitle = L.DomUtil.create("div", "location-search-suggestions-title", this.entitySuggestions);
        entitySuggestionsTitle.innerHTML = multilangHtml("Personnes morales", "Legal entities");
        this.entitySuggestionsContent = L.DomUtil.create("div", "location-search-suggestions-content", this.entitySuggestions);
        
        // address suggestions
        this.addressSuggestions = L.DomUtil.create("div", null, this.suggestions);
        this.addressSuggestions.id = "location-search-suggestions-addresses";
        const addressSuggestionsTitle = L.DomUtil.create("div", "location-search-suggestions-title", this.addressSuggestions);
        addressSuggestionsTitle.innerHTML = multilangHtml("Adresses", "Addresses");
        this.addressSuggestionsContent = L.DomUtil.create("div", "location-search-suggestions-content", this.addressSuggestions);
        
        // status
        this.status = L.DomUtil.create("div", null, container);
        this.status.id = "location-search-status";
        L.DomEvent.disableClickPropagation(this.status);
        
        // zoom warning
        this.zoomWarning = L.DomUtil.create("div", null, container);
        this.zoomWarning.id = "location-search-zoom-warning";
        L.DomEvent.disableClickPropagation(this.zoomWarning);
        this.zoomWarning.innerHTML = multilangHtml(
            "La vue actuelle contient trop d'emplacements. Merci de zoomer pour activer l'affichage des parcelles et adresses.",
            "Current view contains too many locations. Please zoom to enable the display of plots and addresses."
        );
        
        // return the container
        return container;
    },
    
    // updates input select field, making it appear or disappear
    updateInputSelectField(){
        // create a search object
        const search = this.createSearch();
        
        // if the user is searching for a big entity
        if(search !== null && search.bigEntity !== null){
            // show the input select field and reset its options
            this.inputSelectField.style.display = "block";
            this.inputSelectField.innerHTML = "";
            this.inputSelectField.value = "";
            
            // add first options
            const firstOption = L.DomUtil.create("option", null, this.inputSelectField);
            firstOption.value = "";
            firstOption.innerHTML = "Département";
            
            // add departments in which the big entity is present
            const departments = new Map(Object.entries(search.bigEntity).sort());
            for(const [departement, number] of departments){
                const option = L.DomUtil.create("option", null, this.inputSelectField);
                option.value = departement;
                option.innerHTML = `${departement} - ${squatMap.departements.get(departement)} (${number})`;
            }
        }
        // else, if the user isn't searching for a big entity
        else{
            // hide the input select field
            this.inputSelectField.style.display = "none";
        }
    },
    
    // resets displayed suggestions
    resetSuggestions(){
        this.suggestions.style.display = "none";
        this.addressSuggestionsContent.innerHTML = "";
        this.entitySuggestionsContent.innerHTML = "";
    },
    
    // called when the search icon is clicked or when the user presses "Enter" in the search input
    search(){
        // current search isn't ongoing and big entities have been loaded
        if(this.currentSearch.ongoing === false && this.bigEntities !== null){
            // create the new search
            const newSearch = this.createSearch();
            
            // the new search is invalid or is valid but the user didn't specify a department despite searching for a big entity
            if(newSearch === null || (newSearch.bigEntity !== null && newSearch.departement === null)){
                // reset the current search so that its parameters aren't used to color locations that appear on the map anymore
                this.currentSearch.string = null;
                this.currentSearch.bounds = null;
                
                // set the search status
                if(newSearch === null){
                    this.setCurrentSearchStatus(SearchingStatus.noStatus);
                }
                else{
                    this.setCurrentSearchStatus(SearchingStatus.bigEntityDepartementRequired);
                }
                
                // unmatch all location layers
                for(const locationType of [0, 1]){
                    for(const layer of squatMap.locationLayerGroups[locationType].getLayers()){
                        squatMap.setLocationLayerMatchProperty(layer, false);
                    }
                }
                
                // remove all markers
                squatMap.markerLayerGroup.clearLayers();
            }
            // the new search is valid and different from the last search
            else if(this.currentSearch.string === null ||
                    newSearch.type !== this.currentSearch.type ||
                    newSearch.string !== this.currentSearch.string ||
                    newSearch.departement !== this.currentSearch.departement){
                // update the current search parameters
                this.currentSearch.type = newSearch.type;
                this.currentSearch.string = newSearch.string;
                this.currentSearch.departement = newSearch.departement;
                this.currentSearch.bounds = null;
                this.currentSearch.ongoing = true;
                this.setCurrentSearchStatus(SearchingStatus.waitingForServer);
                
                // remove all markers
                squatMap.markerLayerGroup.clearLayers();
                
                // set the request URL
                let url = `/get_searched_locations.php?search-type=${this.currentSearch.type.description}&search-string=${this.currentSearch.string}`;
                if(this.currentSearch.departement !== null){
                    url += `&departement=${this.currentSearch.departement}`;
                }

                // make the request
                const abortController = new AbortController();
                const timeoutId = setTimeout(
                    () => {
                        abortController.abort();
                        this.setCurrentSearchStatus(SearchingStatus.serverError);
                    },
                    this.searchRequestTimeout
                );
                fetch(url, {"signal": abortController.signal}).then((response) => {
                    // no client timeout
                    clearTimeout(timeoutId);
                    
                    // server timeout
                    if(response.status === 504){
                        return null;
                    }
                    
                    return response.json();
                }).then((json) => {
                    // valid JSON
                    if(json !== null){
                        // locations found
                        if(json["locations"].length > 0){
                            // set searching status
                            this.setCurrentSearchStatus(SearchingStatus.loadingMarkers);
                            
                            // set markers and bounds
                            const markers = new Array();
                            this.currentSearch.bounds = L.latLngBounds();
                            this.currentSearch.plotCount = 0;
                            this.currentSearch.addressCount = 0;
                            for(const location of json["locations"]){
                                const center = [location.lat, location.lng];
                                this.currentSearch.bounds.extend(center);
                                if(location.type === 0){
                                    markers.push(this.createMarker(center, squatMap.plotIcon, location.type, location.id));
                                    this.currentSearch.plotCount += 1;
                                }
                                else if(location.type === 1){
                                    markers.push(this.createMarker(center, squatMap.addressIcon, location.type, location.id));
                                    this.currentSearch.addressCount += 1;
                                }
                            }
                            
                            // fly the map to the bounds
                            this.flyToCurrentSearchBounds();
                            
                            // add markers
                            squatMap.markerLayerGroup.addLayers(markers);
                        }
                        // no locations found
                        else{
                            // search is no longer ongoing
                            this.currentSearch.ongoing = false;
                            this.setCurrentSearchStatus(SearchingStatus.noLocationsFound);
                        }
                    }
                    // invalid JSON
                    else{
                        // search is no longer ongoing
                        this.currentSearch.ongoing = false;
                        this.setCurrentSearchStatus(SearchingStatus.serverError);
                    }
                }).catch((error) => {
                    // search is no longer ongoing
                    this.currentSearch.ongoing = false;
                    this.setCurrentSearchStatus(SearchingStatus.serverError);
                });
                    
                // update map locations
                squatMap.updateLocations();
            }
            // the new search is valid and the same as the last search
            else{
                // fly again to the current search bounds
                this.flyToCurrentSearchBounds();
            }
        }
    },
    
    // changes the value of the input field and searches for the string
    searchString(string){
        if(this.inputField.value !== string){
            this.inputField.value = string;
            this.updateInputSelectField();
        }
        this.search();
    },
    
    // sets the searching status and updates the displayed status accordingly
    setCurrentSearchStatus(newStatus, stepCompletionRatio = 0){
        this.currentSearch.status = newStatus;
        
        if(this.currentSearch.status == SearchingStatus.noStatus){
            this.status.style.display = "none";
        }
        else{
            this.status.style.display = "block";
            if(this.currentSearch.status === SearchingStatus.waitingForServer){
                this.status.innerHTML = multilangHtml("En attente du serveur...", "Waiting for server...");
            }
            else if(this.currentSearch.status === SearchingStatus.serverError){
                this.status.innerHTML = multilangHtml("Erreur serveur, merci de ré-essayer plus tard.", "Server error, please try again later.");
            }
            else if(this.currentSearch.status === SearchingStatus.noLocationsFound){
                this.status.innerHTML = multilangHtml("Aucun résultat trouvé.", "No results found.");
            }
            else if(this.currentSearch.status === SearchingStatus.loadingMarkers){
                this.status.innerHTML = multilangHtml("Chargement des résultats...", "Loading results...") + " " + Math.floor(stepCompletionRatio * 100) + "%";
            }
            else if(this.currentSearch.status === SearchingStatus.markersLoaded){
                this.status.innerHTML = "";
                if(this.currentSearch.plotCount === 1){
                    this.status.innerHTML += multilangHtml(
                        '<span class="location-search-plot-text">1 parcelle</span> ',
                        '<span class="location-search-plot-text">1 plot</span> '
                    );
                }
                else if(this.currentSearch.plotCount > 1){
                    this.status.innerHTML += multilangHtml(
                        `<span class="location-search-plot-text">${this.currentSearch.plotCount} parcelles</span> `,
                        `<span class="location-search-plot-text">${this.currentSearch.plotCount} plots</span> `
                    );
                }
                if(this.currentSearch.plotCount !== 0 && this.currentSearch.addressCount !== 0){
                    this.status.innerHTML += multilangHtml("et ", "and ");
                }
                if(this.currentSearch.addressCount === 1){
                    this.status.innerHTML += multilangHtml(
                        '<span class="location-search-address-text">1 adresse</span> ',
                        '<span class="location-search-address-text">1 address</span> '
                    );
                }
                else if(this.currentSearch.addressCount > 1){
                    this.status.innerHTML += multilangHtml(
                        `<span class="location-search-address-text">${this.currentSearch.addressCount} adresses</span> `,
                        `<span class="location-search-address-text">${this.currentSearch.addressCount} addresses</span> `
                    );
                }
                if(this.currentSearch.plotCount + this.currentSearch.addressCount === 1){
                    this.status.innerHTML += multilangHtml("trouvée.", "found.");
                }
                else{
                    this.status.innerHTML += multilangHtml("trouvées.", "found.");
                }
            }
            else if(this.currentSearch.status === SearchingStatus.bigEntityDepartementRequired){
                this.status.innerHTML = multilangHtml(
                    "La personne morale demandée étant liée à un grand nombre d'emplacements, merci de préciser votre recherche en sélectionnant un département dans la barre de recherche.",
                    "The requested legal entity is linked to a large number of locations, please refine your search by selecting a department in the search bar."
                );
            }
        }
    },
    
    // called when the user types in the search input
    updateSuggestions(){
        // a request for suggestions isn't already ongoing
        if(this.suggesting === false){
            // set the search string (trim it and remove diacritics)
            const searchString = this.inputField.value.trim().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            
            // at least one non-whitespace characters
            if(searchString.length >= 1){
                // a request for suggestions is now ongoing
                this.suggesting = true;
                
                // set the request ID
                const requestId = this.nextSuggestionRequestId;
                this.nextSuggestionRequestId += 1;
                
                // set the request URL
                const url = `/get_suggestions.php?search-string=${searchString}`;

                // make the request
                const abortController = new AbortController();
                const timeoutId = setTimeout(
                    () => {
                        abortController.abort();
                    },
                    this.suggestionsRequestTimeout
                );
                fetch(url, {"signal": abortController.signal}).then((response) => {
                    // no client timeout
                    clearTimeout(timeoutId);
                    
                    // server timeout
                    if(response.status === 504){
                        this.suggesting = false;
                        this.resetSuggestions();
                        if(this.chainSuggestionRequest === true){
                            this.chainSuggestionRequest = false;
                            this.updateSuggestions();
                        }
                        return null;
                    }
                    
                    return response.json();
                }).then((json) => {
                    // reset suggestions
                    this.resetSuggestions();
                    
                    // the request is the last request, and JSON is valid
                    if(requestId === this.nextSuggestionRequestId - 1 && json !== null){
                        this.suggestions.style.display = "block";
                        
                        // set address suggestions
                        if(json["addresses"].length > 0){
                            for(const address of json["addresses"]){
                                // craft the highlighted text with diacritics
                                const highlightedWithDiacritics = transferBoldTags(address.highlighted_text.replace(" ", ""), address.original_text);
                                
                                // add the suggestion
                                const suggestion = L.DomUtil.create("button", "location-search-suggestions-content-suggestion", this.addressSuggestionsContent);
                                suggestion.tabIndex = 0;
                                suggestion.addEventListener("click", () => { this.searchString(address.original_text); });
                                const suggestionDescription = L.DomUtil.create("div", "location-search-suggestions-content-suggestion-description", suggestion);
                                suggestionDescription.innerHTML = highlightedWithDiacritics;
                            }
                        }

                        // set entity suggestions
                        if(json["entities"].length > 0){
                            for(const entity of json["entities"]){
                                const suggestion = L.DomUtil.create("button", "location-search-suggestions-content-suggestion", this.entitySuggestionsContent);
                                suggestion.tabIndex = 0;
                                suggestion.addEventListener("click", () => { this.searchString(entity.number); });
                                const suggestionDescription = L.DomUtil.create("div", "location-search-suggestions-content-suggestion-description", suggestion);
                                suggestionDescription.innerHTML = `${entity.name} (${entity.number})`;
                                const suggestionNumber = L.DomUtil.create("div", "location-search-suggestions-content-suggestion-numbers", suggestion);
                                suggestionNumber.innerHTML = "";
                                if(entity.plots !== 0){
                                    suggestionNumber.innerHTML += `<div class="location-search-suggestions-content-suggestion-number location-search-plot-text">${entity.plots}</span>`;
                                }
                                if(entity.addresses !== 0){
                                    suggestionNumber.innerHTML += `<div class="location-search-suggestions-content-suggestion-number location-search-address-text">${entity.addresses}</span>`;
                                }
                            }
                        }
                    }
                    
                    this.suggesting = false;
                    if(this.chainSuggestionRequest === true){
                        this.chainSuggestionRequest = false;
                        this.updateSuggestions();
                    }
                }).catch((error) => {
                    this.suggesting = false;
                    this.resetSuggestions();
                    if(this.chainSuggestionRequest === true){
                        this.chainSuggestionRequest = false;
                        this.updateSuggestions();
                    }
                });
            }
            else{
                this.resetSuggestions();
            }
        }
        else{
            this.resetSuggestions();
            this.chainSuggestionRequest = true;
        }
    }
});

L.control.locationSearch = function(opts){
    return new L.Control.LocationSearch(opts);
}
