<?php
    abstract class LocationsQueryType{
        const GetGeolocatedLocations = 0; // get the locations that match the geolocation parameters of the GET request (longitude and latitude)
        const GetSearchedLocations = 1; // get the locations that match the search parameters of the GET request (address, entity number...)
    }
    
    class LocationsQuery{
        public $parameters = [];
        public $sql = "";
        public $type;
        
        const MAX_GEOLOCATED_LOCATIONS = 2000;
        
        // constructor
        public function __construct($type){
            // set type
            $this->type = $type;
            
            // craft the SQL condition based on the search parameters
            $condition = "false";
            $conditionPieces = [];
            if(!empty($_GET['search-string']) && is_string($_GET['search-string'])){
                if(!empty($_GET['search-type']) && $_GET['search-type'] === 'address'){
                    array_push($this->parameters, $_GET['search-string']);
                    array_push($conditionPieces, sprintf("location.address_text = $%d", sizeof($this->parameters)));
                }
                else if(!empty($_GET['search-type']) && $_GET['search-type'] === 'entity'){
                    array_push($this->parameters, $_GET['search-string']);
                    array_push($conditionPieces, sprintf("location.entity_number = $%d", sizeof($this->parameters)));
                    if(!empty($_GET['departement']) && is_string($_GET['departement'])){
                        array_push($this->parameters, $_GET['departement']);
                        array_push($conditionPieces, sprintf("location.departement = $%d", sizeof($this->parameters)));
                    }
                }
                else if(!empty($_GET['search-type']) && $_GET['search-type'] === 'cadastralReference'){
                    array_push($this->parameters, substr($_GET['search-string'], 0, 5));
                    array_push($conditionPieces, sprintf("location.plot_commune = $%d", sizeof($this->parameters)));
                    array_push($this->parameters, substr($_GET['search-string'], 5, 3));
                    array_push($conditionPieces, sprintf("location.plot_prefixe = $%d", sizeof($this->parameters)));
                    array_push($this->parameters, substr($_GET['search-string'], 8, 2));
                    array_push($conditionPieces, sprintf("location.plot_section = $%d", sizeof($this->parameters)));
                    array_push($this->parameters, substr($_GET['search-string'], 10, 4));
                    array_push($conditionPieces, sprintf("location.plot_numero = $%d", sizeof($this->parameters)));
                }
            }
            if(sizeof($conditionPieces) !== 0){
                $condition = implode(" and ", $conditionPieces);
            }

            // craft the SQL query
            if($this->type === LocationsQueryType::GetSearchedLocations){
                $this->sql = "select location.type, location.id, location.center_latitude as lat, location.center_longitude as lng";
            }
            else if($this->type === LocationsQueryType::GetGeolocatedLocations){
                $this->sql = "
                    select location.type,
                           location.id,
                           location.departement,
                           location.plot_commune,
                           location.plot_prefixe,
                           location.plot_section,
                           location.plot_numero,
                           location.plot_commune_name,
                           AsGeoJSON(location.plot_geography) as plot_geography,
                           location.address_text,
                           AsGeoJSON(location.address_geography) as address_geography,
                           location.entity_number,
                           location.entity_relation,
                           location.entity_name,
                           location.entity_amount,
                           location.entity_year,
                           location.entity_month,
                           location.entity_details,
                           case when ({$condition}) then 1 else 0 end as match
                ";
            }
            
            $this->sql .= " from location";
            
            if($this->type === LocationsQueryType::GetGeolocatedLocations){
                array_push($this->parameters, $_GET['lat1']);
                array_push($this->parameters, $_GET['lat0']);
                array_push($this->parameters, $_GET['lng0']);
                array_push($this->parameters, $_GET['lng1']);
                $this->sql .= sprintf(
                    " where
                        (location.type = 0 and location.rowid in (select pkid from idx_location_plot_geography where xmax >= $%d and xmin <= $%d and ymax >= $%d and ymin <= $%d)) or
                        (location.type = 1 and location.rowid in (select pkid from idx_location_address_geography where xmax >= $%d and xmin <= $%d and ymax >= $%d and ymin <= $%d))
                      limit %d;",
                    sizeof($this->parameters) - 3,
                    sizeof($this->parameters) - 2,
                    sizeof($this->parameters) - 1,
                    sizeof($this->parameters),
                    sizeof($this->parameters) - 3,
                    sizeof($this->parameters) - 2,
                    sizeof($this->parameters) - 1,
                    sizeof($this->parameters),
                    LocationsQuery::MAX_GEOLOCATED_LOCATIONS
                );
            }
            else if($this->type === LocationsQueryType::GetSearchedLocations){
                $this->sql .= " where {$condition} group by location.id;";
            }
        }
        
        // opens a new database connection, executes the query and returns the result as a JSON string
        public function execute(){
            // connect to the database
            $connection = new SQLite3('../../database.db', SQLITE3_OPEN_READONLY);
            
            // load big entities
            $bigEntities = json_decode(file_get_contents('big_entities.json'), true);
            
            // return no results if a big entity is being searched and no departement is provided
            if($this->type === LocationsQueryType::GetSearchedLocations &&
               !empty($_GET['search-string']) &&
               is_string($_GET['search-string']) &&
               !empty($_GET['search-type']) &&
               $_GET['search-type'] === 'entity' &&
               array_key_exists($_GET['search-string'], $bigEntities) &&
               empty($_GET['departement'])
            ){
                return json_encode(array("locations" => []));
            }
            
            // load Spatialite
            $connection->loadExtension('mod_spatialite.so');
            
            // prepare the statement
            $statement = $connection->prepare($this->sql);
            foreach($this->parameters as $index => $parameter){
                $statement->bindValue($index + 1, $parameter);
            }
            
            // execute the statement
            $result = $statement->execute();
            
            // get the locations
            $locations = array();
            while($location = $result->fetchArray(SQLITE3_ASSOC)){
                array_push($locations, $location);
            }
            
            // close the database connection
            $connection->close();
            
            // return the locations
            if($locations !== false){
                if($this->type === LocationsQueryType::GetGeolocatedLocations && sizeof($locations) === LocationsQuery::MAX_GEOLOCATED_LOCATIONS){
                    return json_encode(array("error" => "too-many-locations", "locations" => []));
                }
                return json_encode(array("locations" => $locations));
            }
            return json_encode(array("locations" => []));
        }
    }
?>
