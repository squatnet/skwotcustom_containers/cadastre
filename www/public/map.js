// creates an enumeration
function createEnum(arr){
    let obj = Object.create(null);
    for (let val of arr){
        obj[val] = Symbol(val);
    }
    return Object.freeze(obj);
}

// returns the width and height of an HTML element that hasn't been appended to the DOM yet
function measureElement(element){
    // change position and visibility
    const previousPosition = element.style.position;
    const previousVisibility = element.style.visibility;
    element.style.visibility = "hidden";
    element.style.position = "absolute";
    
    // briefly add to DOM and measure
    document.body.appendChild(element);
    size = new Array(element.offsetWidth, element.offsetHeight);
    element.parentNode.removeChild(element);
    
    // restore previous position and visibility
    element.style.position = previousPosition;
    element.style.visibility = previousVisibility;
    
    // return size
    return size;
}

// returns the HTML that represents a multilang string
function multilangHtml(french, english){
    return "<span class=\"language-fr\">" + french + "</span>" + "<span class=\"language-en\">" + english + "</span>";
}

// transfers sets of <b> and </b> tags from a string to another
// the second string must be the same length as the first string if it didn't have the sets of tags
// the tags must exactly be "<b>" and "</b>" (no additional spaces, etc.)
// returns the second string, with the tags
function transferBoldTags(firstString, secondString){
    for(const match of Array.from(firstString.matchAll(/<b>.*?<\/b>/g))){
        secondString = secondString.substring(0, match.index) +
                       "<b>" +
                       secondString.substring(match.index, match.index + match[0].length - 7) +
                       "</b>" +
                       secondString.substring(match.index + match[0].length - 7);
    }
    return secondString;
}

const SearchingStatus = createEnum(["noStatus", "waitingForServer", "serverError", "noLocationsFound", "loadingMarkers", "markersLoaded", "bigEntityDepartementRequired"]);
const SearchType = createEnum(["address", "entity", "cadastralReference"]);

class SquatMap{
    // adds descriptions of some entities to an HTML element
    addEntitiesDescriptions(entities, container){
        for(const entities_array of entities.values()){
            // add the description
            const locationDescriptionEntity = L.DomUtil.create("div", "location-description-entity", container);
            const locationDescriptionEntityName = L.DomUtil.create("div", "location-description-entity-name", locationDescriptionEntity);
            const beforeNumber = L.DomUtil.create("span", null, locationDescriptionEntityName);
            beforeNumber.innerHTML = entities_array[0].name + " (";
            const internalSearchLink = L.DomUtil.create("span", "internal-search-link", locationDescriptionEntityName);
            internalSearchLink.innerHTML = entities_array[0].number;
            internalSearchLink.addEventListener("click", () => { this.locationSearch.searchString(entities_array[0].number); });
            const afterNumber = L.DomUtil.create("span", null, locationDescriptionEntityName);
            afterNumber.innerHTML = ")";
            
            // add relation texts
            for(const entity of entities_array){
                // set the relation text
                let relationText;
                if(entity.relation === 100 || entity.relation === 101){
                    if(entity.amount === 1){
                        // entity has one headquarters or establishment at this address
                        relationText = squatMap.entityRelationToHTML.get(entity.relation)[0];
                    }
                    else{
                        // entity has several headquarters or establishments at this address
                        relationText = entity.amount.toString() + " " + squatMap.entityRelationToHTML.get(entity.relation)[1];
                    }
                    
                    if(entity.details !== null){
                        // details are available on the headquarters/establishment
                        relationText += ` "${entity.details}"`;
                    }
                }
                else{
                    if(entity.details !== null){
                        // entity is related to a plot and the relation is about specific premises on the plot
                        relationText = squatMap.entityRelationToHTML.get(entity.relation) + " ";
                        relationText += multilangHtml("d'un local", "of premises");
                        if(entity.details != ""){
                            relationText += ` "${entity.details}"`;
                        }
                    }
                    else{
                        // entity is related to a plot and the relation is about the whole plot
                        relationText = squatMap.entityRelationToHTML.get(entity.relation);
                    }
                }
                relationText += ` (${entity.month.toString().padStart(2, "0")}/${entity.year})`;
                
                // add the relation text
                const locationDescriptionEntityStatus = L.DomUtil.create("div", "location-description-entity-status", locationDescriptionEntity);
                locationDescriptionEntityStatus.innerHTML = relationText;
            }
        }
    }
    
    // called when an address layer is added, should return the corresponding marker
    addressPointToLayer(feature, latlng){
        return L.circleMarker(latlng, {
            radius: 8,
            opacity: 1
        });
    }
    
    // constructor
    constructor(){        
        // map
        this.map = L.map("map", {
            "zoomControl": false
        }).setView([46.89, 2.22], 6);
        
        // minimum zoom level at which locations are requested from the database (this shouldn't be set too low to avoid requesting too many locations)
        this.locationsMinimumZoom = 15;
        
        // true if locations are being updated
        this.updatingLocations = false;
        
        // true if locations should be updated once the current update is over
        this.chainLocationsUpdate = false;
        
        // selected location layer, shown in red (null if no location layer is selected)
        this.selectedLocationLayer = null;
        
        // maps entities relations to the corresponding multilang HTML
        this.entityRelationToHTML = new Map([
            [0, multilangHtml("Propriétaire", "Owner")],
            [1, multilangHtml("Usufruitier", "Usufructuary")],
            [2, multilangHtml("Nu-propriétaire", "Bare owner")],
            [3, multilangHtml("Bailleur à construction", "Bailleur à construction")],
            [4, multilangHtml("Preneur à construction", "Preneur à construction")],
            [5, multilangHtml("Foncier", "Foncier")],
            [6, multilangHtml("Ténuyer", "Ténuyer")],
            [7, multilangHtml("Domanier", "Domanier")],
            [8, multilangHtml("Bailleur d'un bail à réhabilitation", "Bailleur d'un bail à réhabilitation")],
            [9, multilangHtml("Preneur d'un bail à réhabilitation", "Preneur d'un bail à réhabilitation")],
            [10, multilangHtml("Locataire-Attributaire", "Locataire-Attributaire")],
            [11, multilangHtml("Emphytéote", "Emphytéote")],
            [12, multilangHtml("Antichrésiste", "Antichrésiste")],
            [13, multilangHtml("Fonctionnaire logé", "Housed civil servant")],
            [14, multilangHtml("Gérant/mandataire", "Manager/representative")],
            [15, multilangHtml("Syndic de copropriété", "Co-ownership trustee")],
            [16, multilangHtml("Associé dans une société en transparence fiscale", "Associé dans une société en transparence fiscale")],
            [17, multilangHtml("Autorisation d’occupation temporaire", "Temporary occupation authorization")],
            [18, multilangHtml("Jeune agriculteur", "Young farmer")],
            [19, multilangHtml("Gestionnaire taxe sur les bureaux", "Gestionnaire taxe sur les bureaux")],
            [20, multilangHtml("La Poste occupant et propriétaire", "La Poste occupant and owner")],
            [21, multilangHtml("La Poste occupant et non propriétaire", "La Poste occupant but not owner")],
            [22, multilangHtml("Fiduciaire", "Fiduciary")],
            [23, multilangHtml("Occupant d’une parcelle appartenant au département de Mayotte ou à l’Etat", "Owner of a plot belonging to the Mayotte département or the State")],
            [24, multilangHtml("Gestionnaire d’un bien de l’État", "Manager of a State property")],
            [
                100,
                [multilangHtml("Siège social", "Headquarters"), multilangHtml("Siège sociaux", "Headquarters")]
            ],
            [
                101,
                [multilangHtml("Établissement", "Establishment"), multilangHtml("Établissements", "Establishments")]
            ],
        ]);
        
        // maps departements number to names
        this.departements = new Map([
            ["01", "Ain"],
            ["02", "Aisne"],
            ["03", "Allier"],
            ["04", "Alpes-de-Haute-Provence"],
            ["05", "Hautes-Alpes"],
            ["06", "Alpes-Maritimes"],
            ["07", "Ardèche"],
            ["08", "Ardennes"],
            ["09", "Ariège"],
            ["10", "Aube"],
            ["11", "Aude"],
            ["12", "Aveyron"],
            ["13", "Bouches-du-Rhône"],
            ["14", "Calvados"],
            ["15", "Cantal"],
            ["16", "Charente"],
            ["17", "Charente-Maritime"],
            ["18", "Cher"],
            ["19", "Corrèze"],
            ["2A", "Corse-du-Sud"],
            ["2B", "Haute-Corse"],
            ["21", "Côte-d'Or"],
            ["22", "Côtes d'Armor"],
            ["23", "Creuse"],
            ["24", "Dordogne"],
            ["25", "Doubs"],
            ["26", "Drôme"],
            ["27", "Eure"],
            ["28", "Eure-et-Loir"],
            ["29", "Finistère"],
            ["30", "Gard"],
            ["31", "Haute-Garonne"],
            ["32", "Gers"],
            ["33", "Gironde"],
            ["34", "Hérault"],
            ["35", "Ille-et-Vilaine"],
            ["36", "Indre"],
            ["37", "Indre-et-Loire"],
            ["38", "Isère"],
            ["39", "Jura"],
            ["40", "Landes"],
            ["41", "Loir-et-Cher"],
            ["42", "Loire"],
            ["43", "Haute-Loire"],
            ["44", "Loire-Atlantique"],
            ["45", "Loiret"],
            ["46", "Lot"],
            ["47", "Lot-et-Garonne"],
            ["48", "Lozère"],
            ["49", "Maine-et-Loire"],
            ["50", "Manche"],
            ["51", "Marne"],
            ["52", "Haute-Marne"],
            ["53", "Mayenne"],
            ["54", "Meurthe-et-Moselle"],
            ["55", "Meuse"],
            ["56", "Morbihan"],
            ["57", "Moselle"],
            ["58", "Nièvre"],
            ["59", "Nord (Lille"],
            ["60", "Oise"],
            ["61", "Orne"],
            ["62", "Pas-de-Calais"],
            ["63", "Puy-de-Dôme"],
            ["64", "Pyrénées-Atlantiques"],
            ["65", "Hautes-Pyrénées"],
            ["66", "Pyrénées-Orientales"],
            ["67", "Bas-Rhin"],
            ["68", "Haut-Rhin"],
            ["69", "Rhône"],
            ["70", "Haute-Saône"],
            ["71", "Saône-et-Loire"],
            ["72", "Sarthe"],
            ["73", "Savoie"],
            ["74", "Haute-Savoie"],
            ["75", "Paris"],
            ["76", "Seine-Maritime"],
            ["77", "Seine-et-Marne"],
            ["78", "Yvelines"],
            ["79", "Deux-Sèvres"],
            ["80", "Somme"],
            ["81", "Tarn"],
            ["82", "Tarn-et-Garonne"],
            ["83", "Var"],
            ["84", "Vaucluse"],
            ["85", "Vendée"],
            ["86", "Vienne"],
            ["87", "Haute-Vienne"],
            ["88", "Vosges"],
            ["89", "Yonne"],
            ["90", "Territoire de Belfort"],
            ["91", "Essonne"],
            ["92", "Hauts-de-Seine"],
            ["93", "Seine-Saint-Denis"],
            ["94", "Val-de-Marne"],
            ["95", "Val-d'Oise"],
            ["971", "Guadeloupe"],
            ["972", "Martinique"],
            ["973", "Guyane"],
            ["974", "La Réunion"],
            ["976", "Mayotte"]
        ]);
    }
    
    // called when a chunk of markers has been added
    chunkProgress(processed, total, elapsed, layersArray){
        if(this.locationSearch !== undefined){
            this.locationSearch.chunkProgress(processed, total, elapsed, layersArray);
        }
    }
    
    // creates an icon that represents a cluster of location markers
    createClusterIcon(cluster){
        // count markers of each type
        const markerCount = new Array(0, 0);
        for(const marker of cluster.getAllChildMarkers()){
            markerCount[marker.options.type] += 1;
        }
        
        // create the icon content
        const iconContent = L.DomUtil.create("div", "location-cluster-icon-content");
        if(markerCount[0] !== 0){
            const iconPlots = L.DomUtil.create("div", "location-cluster-icon-locations location-cluster-icon-plots", iconContent);
            iconPlots.innerHTML = markerCount[0];
        }
        if(markerCount[1] !== 0){
            const iconAddresses = L.DomUtil.create("div", "location-cluster-icon-locations location-cluster-icon-addresses", iconContent);
            iconAddresses.innerHTML = markerCount[1];
        }
        
        // measure the icon content size if it was added to the DOM
        const iconContentSize = measureElement(iconContent);
        
        // return the icon
        return L.divIcon({
            "className": "location-cluster-icon",
            "html": iconContent,
            "iconSize": iconContentSize
        });
    }
    
    // creates a location layer popup if it hasn't been created already
    createLocationLayerPopup(layer, latlng){
        // create, bind and open the popup (it is created only on the first click for optimization reasons)
        if(layer.getPopup() === undefined || layer.getPopup() === null){
            let popupContent;
            if(layer.feature.properties.type === 0){
                popupContent = L.DomUtil.create("div", "location-description");
                const plotDescriptionLocation = L.DomUtil.create("span", "location-description-title", popupContent);
                plotDescriptionLocation.innerHTML = `${layer.feature.properties.communeName} (${layer.feature.properties.departement}) · ${layer.feature.properties.commune} ${layer.feature.properties.prefixe} ${layer.feature.properties.section} ${layer.feature.properties.numero}`;
                this.addEntitiesDescriptions(layer.feature.properties.entities, popupContent);
            }
            else if(layer.feature.properties.type === 1){
                popupContent = L.DomUtil.create("div", "location-description");
                const commaIndex = layer.feature.properties.text.lastIndexOf(",");
                const addressDescriptionLocation = L.DomUtil.create("span", "location-description-title", popupContent);
                addressDescriptionLocation.innerHTML = layer.feature.properties.text.slice(0, commaIndex) + "<br/>" + layer.feature.properties.text.slice(commaIndex + 2);
                this.addEntitiesDescriptions(layer.feature.properties.entities, popupContent);
            }
            layer.bindPopup(popupContent).openPopup(latlng);
        }
    }
    
    // initializes the map
    initialize(){
        // hash control
        this.hash = new L.Hash(this.map);
        
        // attribution
        this.map.attributionControl.setPrefix(`
&copy;&nbsp;<a href="https://openstreetmap.org/copyright" target="_blank">OpenStreetMap</a> |
&copy;&nbsp;<a href="https://geoservices.ign.fr/parcellaire-express-pci" target="_blank">IGN</a> |
&copy;&nbsp;<a href="https://data.economie.gouv.fr/explore/dataset/fichiers-des-locaux-et-des-parcelles-des-personnes-morales" target="_blank">Ministère de l\'Économie</a> |
&copy;&nbsp;<a href="https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret" target="_blank">INSEE</a> |
&copy;&nbsp;<a href="https://data.cquest.org/geo_sirene/v2019" target="_blank">Géo-SIRENE</a> |
&copy;&nbsp;<a href="https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer">Esri, Maxar, Earthstar Geographics, and the GIS User Community</a>
        `);
        
        // address icon
        this.addressIcon = L.icon({
            "className": "location-point-icon",
            "iconUrl": "address-icon.png",
            "iconSize": [25, 41],
            "iconAnchor": [12, 41]
        });
        
        // plot icon
        this.plotIcon = L.icon({
            "className": "location-point-icon",
            "iconUrl": "plot-icon.png",
            "iconSize": [25, 41],
            "iconAnchor": [12, 41]
        });
        
        // tile layer
        this.openStreetMap = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            "attribution": '',
            "maxNativeZoom": 19,
            "maxZoom": 20
        }).addTo(this.map);
        
        // first satellite layer
        this.firstSatellite = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            "attribution": '',
            "maxNativeZoom": 19,
            "maxZoom": 20
        });
        
        // second satellite layer
        this.secondSatellite = L.tileLayer('https://mt0.google.com/vt?lyrs=s&x={x}&s=&y={y}&z={z}', {
            "attribution": '',
            "maxZoom": 20
        });
        
        // location layer groups
        this.locationLayerGroups = new Array(
            // plot layer group
            L.geoJSON(
                null,
                {
                    "coordsToLatLng": this.invertLatLng.bind(this),
                    "onEachFeature": this.onEachLocation.bind(this)
                }
            ).addTo(this.map),
            // address layer group
            L.geoJSON(
                null,
                {
                    "coordsToLatLng": this.invertLatLng.bind(this),
                    "pointToLayer": this.addressPointToLayer.bind(this),
                    "onEachFeature": this.onEachLocation.bind(this)
                }
            ).addTo(this.map)
        );
        this.plotLayerGroup = this.locationLayerGroups[0];
        this.addressLayerGroup = this.locationLayerGroups[1];
        
        // marker layer groups
        this.markerLayerGroup = L.markerClusterGroup({
            "chunkedLoading": true,
            "chunkProgress": this.chunkProgress.bind(this),
            "disableClusteringAtZoom": 19,
            "iconCreateFunction": this.createClusterIcon.bind(this),
            "showCoverageOnHover": false,
            "spiderfyOnMaxZoom": false
        }).addTo(this.map);
        
        // tile layer control
        L.control.tileLayerSwitcher({position: "topright"}).addTo(this.map);
        
        // language switch control
        L.control.languageSwitcher({position: "topright"}).addTo(this.map);
        
        // zoom control
        L.control.zoom({position: "topright"}).addTo(this.map);
        
        // scale control
        L.control.scale({"imperial": false}).addTo(this.map);
        
        // "about" popup window
        this.aboutWindow = L.control.popupWindow({
            "content": multilangHtml(`
<h2>À propos</h2>
En France, le <i>cadastre</i> est un registre dressant l'état de la propriété foncière à l'échelle du pays. Le site officiel du gouvernement français permettant de consulter la version numérique de ce registre, <a href="https://cadastre.gouv.fr" target="_blank">cadastre.gouv.fr</a>, est franchement nul : la recherche est pénible, le fond de carte est désagréable, et surtout il n'est pas possible de consulter les propriétaires des parcelles.
<br/><br/>
La présente carte se veut un meilleur cadastre en ligne, à destination des activistes, squatteur·euse·s, anarchistes et autres hurluberlus. Elle vise à répondre à des questions telles que "<i>Qui est propriétaire de ce bâtiment ?</i>" ou "<i>Où est-ce que cette entreprise est présente dans cette région ?</i>". Elle référence actuellement :
<ul>
  <li>Toutes les parcelles cadastrales et les adresses postales en France, métropole et outre-mer.</li>
  <li>Les propriétaires et gestionnaires de parcelles, ou de locaux situés sur des parcelles.</li>
  <li>Les établissements enregistrés à des adresses postales.</li>
</ul>
À quelques exceptions près, la carte ne référence que les personnes morales (entreprises, associations, institutions publiques...) et non les particuliers, les informations concernant les particuliers n'étant pas accessibles au public pour des raisons de protection de la vie privée.
<br/><br/>
Pour contacter les cartographes amateur·e·s à l'origine de cette carte, envoyez un mail à hm123@riseup.net.
`,`
<h2>About</h2>
In France, the <i>cadastre</i> is a registry of the state of land ownership at the national level. The official website of the French government where you can consult the digital version of this registry, <a href="https://cadastre.gouv.fr" target="_blank">cadastre.gouv.fr</a>, is quite bad: the search is painful, the map background is unpleasant, and above all it is not possible to consult the owners of the plots.
<br/><br/>
The present map is meant to be a better online cadastre, for activists, squatters, anarchists and other weirdos. It aims to answer questions like "<i>Who owns this building?</i>" or "<i>Where is this company present in this region?</i>". It currently references:
<ul>
  <li>All cadastral plots and postal addresses in France ; both mainland and overseas.</li>
  <li>Owners and managers of plots, or of premises located on plots.</li>
  <li>Establishments registered at postal addresses.</li>
</ul>
With some exceptions, the map only references legal entities (companies, associations, public institutions...) as opposed to private individuals, as information regarding private individuals isn't publicly available for privacy reasons.
<br/><br/>
To contact the amateur cartographers who maintain this map, send an email to hm123@riseup.net.
`),
            "linkText": multilangHtml("À propos", "About")
        });
        this.aboutWindow.addTo(this.map);
        
        // "data origin" popup window
        this.faqWindow = L.control.popupWindow({
            "content": multilangHtml(`
<h2>Foire aux questions</h2>
<h3>D'où proviennent les données de la carte ?</h3>
D'un croisement entre :
<ul>
  <li>La <a href="https://bano.openstreetmap.fr" target="_blank">BANO</a> (Base Adresses Nationale Ouverte) d'OpenStreetMap.</li>
  <li>La base de données <a href="https://geoservices.ign.fr/parcellaire-express-pci" target="_blank">Parcellaire Express</a> de l'IGN.</li>
  <li>Les <a href="https://data.economie.gouv.fr/explore/dataset/fichiers-des-locaux-et-des-parcelles-des-personnes-morales" target="_blank">fichiers des locaux et parcelles des personnes morales</a> du Ministère de l'Économie.</li>
  <li>La base de données <a href="https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret" target="_blank">SIRENE</a> de l'INSEE.</li>
  <li>La base de données <a href="https://data.cquest.org/geo_sirene" target="_blank">Géo-SIRENE</a> de Christian Quest.</li>
</ul>
Le fond de carte OpenStreetMap provient d'<a href="https://openstreetmap.org" target="_blank">OpenStreetMap</a>, les fonds de carte satellites proviennent d'Esri et de Google.
<h3>Que signifient les codes entre parenthèses après les noms des entités&nbsp;? Pourquoi certains commencent par la lettre <i>U</i>&nbsp;?</h3>
Il existe deux cas de figure :
<ul>
  <li>Si l'entité possède un <a href="https://fr.wikipedia.org/wiki/Système_d'identification_du_répertoire_des_entreprises" target="_blank">numéro SIREN</a> comme c'est le cas de nombreuses personnes morales alors le code, composé de neuf chiffres, correspond au numéro SIREN de l'entité.</li>
  <li>Si l'entité ne possède pas de numéro SIREN comme c'est le cas de certaines associations et institutions publiques alors le code, composé de la lettre <i>U</i> suivie de huit chiffres, n'a pas de signification particulière (il s'agit d'un numéro fictif utilisé par la Direction Générale des Finances Publiques pour identifier l'entité).</li>
</ul>
Dans les deux cas, au sein de la carte, un code identifie une entité de manière unique et peut être utilisé pour rechercher des emplacements en lien avec elle.
<h3>Est-ce que le code source de la carte est accessible ?</h3>
Oui ! Le code source de la carte est hébergé sur un <a href="https://0xacab.org/squatnet/skwotcustom_containers/cadastre" target="_blank">dépôt git</a>.
`,`
<h2>Frequently asked questions</h2>
<h3>Where does the map data come from?</h3>
From a cross-reference of:
<ul>
  <li>OpenStreetMap's <a href="https://bano.openstreetmap.fr" target="_blank">BANO</a> (Base Adresses Nationale Ouverte).</li>
  <li>IGN's <a href="https://geoservices.ign.fr/parcellaire-express-pci" target="_blank">Parcellaire Express</a> database.</li>
  <li>The <a href="https://data.economie.gouv.fr/explore/dataset/fichiers-des-locaux-et-des-parcelles-des-personnes-morales" target="_blank">files of premises and plots of legal entities</a> from the Ministry of Economy.</li>
  <li>INSEE's <a href="https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret" target="_blank">SIRENE</a> database.</li>
  <li>Christian Quest's <a href="https://data.cquest.org/geo_sirene" target="_blank">Géo-SIRENE</a> database.</li>
</ul>
The OpenStreetMap map background comes from <a href="https://openstreetmap.org" target="_blank">OpenStreetMap</a>, the satellite map backgrounds come from Esri and Google.
<h3>What do the codes in parentheses after the entities' names mean? Why do some of them start with the letter <i>U</i>?</h3>
There are two cases:
<ul>
<li>If the entity has a <a href="https://en.wikipedia.org/wiki/SIREN_code" target="_blank">SIREN number</a>, as is the case for many legal entities, then the code, composed of nine digits, corresponds to the entity's SIREN number.</li>
<li>If the entity does not have a SIREN number, as is the case for certain associations and public institutions, then the code, composed of the letter <i>U</i> followed by eight digits, has no particular meaning (it is a fictitious number used by the General Directorate of Public Finances to identify the entity).</li>
</ul>
In both cases, within the map, a code uniquely identifies an entity and can be used to find locations related to it.
<h3>Is the map source code available?</h3>
Yes! The map source code is hosted on a <a href="https://0xacab.org/squatnet/skwotcustom_containers/cadastre" target="_blank">git repository</a>.
`),
            "linkText": multilangHtml("FAQ", "FAQ")
        });
        this.faqWindow.addTo(this.map);
        
        // "warning" popup window
        this.warningWindow = L.control.popupWindow({
            "content": multilangHtml(`
<h2>Avertissement : chargement de données externes</h2>
Cette carte propose trois fonds de carte :
<ul>
  <li><i>OpenStreetMap</i>, dont les images sont téléchargées depuis les serveurs de la fondation à but non lucratif OpenStreetMap.</li>
  <li><i>Satellite (Esri)</i>, dont les images, de qualité moyenne, sont téléchargées depuis les serveurs de l'entreprise de développement de logiciels de cartographie Environmental Systems Research Institute.</li>
  <li><i>Satellite (Google)</i>, dont les images, de bonne qualité, sont téléchargées depuis les serveurs de Google.</li>
</ul>
L'organisation correspondant au fond de carte actif (par défaut OpenStreetMap) sait quelles images sont téléchargées, et donc quelles zones géographiques sont consultées sur la carte, mais ne peut en aucun cas avoir accès aux recherches effectuées ni aux parcelles cliquées. Dans le cas de Google, ces zones géographiques peuvent alors être liées au compte Google connecté, le cas échéant. <b>Il est fortement recommandé d'utiliser le <a href="https://torproject.org" target="_blank">Navigateur Tor</a> pour la consultation de cette carte.</b>
<br/><br/>
Les boutons en haut à droite de l'interface peuvent être utilisés pour changer de fond de carte.
`,`
<h2>Warning: external data loading</h2>
This map offers two satellite map backgrounds:
<ul>
  <li><i>OpenStreetMap</i>, with images downloaded from the servers of the non-profit foundation OpenStreetMap.</li>
  <li><i>Satellite (Esri)</i>, with medium-quality images downloaded from the servers of the mapping software development company Environmental Systems Research Institute.</li>
  <li><i>Satellite (Google)</i>, with good quality images from the servers of Google.</li>
</ul>
The organization corresponding to the active map background (OpenStreetMap by default) knows which images are being downloaded, and therefore which geographical areas are being viewed on the map, but has no access to the searches performed or the parcels clicked. In the case of Google, these geographical areas can then be linked to the connected Google account, if any. <b>We strongly recommend using the <a href="https://torproject.org" target="_blank">Tor Browser</a> to view this map.</b>
<br/><br/>
Buttons at the top-right of the interface can be used to change the map background.
`),
            "linkText": multilangHtml("Données externes", "External data")
        });
        this.warningWindow.addTo(this.map);
        this.warningWindowDisplayed = false;
        
        // location search control
        this.locationSearch = L.control.locationSearch({position: "topleft"});
        this.locationSearch.addTo(this.map);
        
        // bottom links
        L.control.bottomLinks({position: "bottomright"}).addTo(this.map);
        
        // events
        this.map.on("moveend", this.updateLocations.bind(this));
        this.map.on("zoomend", (event) =>{
            // hide zoom warning
            if(this.map.getZoom() < this.locationsMinimumZoom){
                this.locationSearch.zoomWarning.style.display = "none";
            }
        });
        
        // update locations
        this.updateLocations();
    }
    
    // given coordinates as a Leaflet LatLng object, returns the inverted coordinates
    invertLatLng(coordinates){
        return new L.LatLng(coordinates[0], coordinates[1], coordinates[2]);
    }
    
    // called on each location and its layer when they are added to the map
    onEachLocation(location, layer){
        // style
        this.updateLocationLayerStyle(layer);
        
        // on click
        layer.on("click", (event) => {
            this.createLocationLayerPopup(layer, event.latlng);
        });
        
        // on popup open/close
        layer.on("popupopen", (event) => {
            this.selectLocationLayer(layer);
        });
        layer.on("popupclose", (event) => {
            this.unselectLocationLayer(layer);
        });
    }
    
    // sets the "match" property of a location layer
    setLocationLayerMatchProperty(layer, value){
        if(layer.feature.properties.match !== value){
            layer.feature.properties.match = value;
            this.updateLocationLayerStyle(layer);
        }
    }
    
    // selects a location layer if it is unselected (has no effect otherwise)
    selectLocationLayer(layer){
        if(layer !== this.selectedLocationLayer){
            const oldSelectedLocationLayer = this.selectedLocationLayer;
            this.selectedLocationLayer = layer;
            if(oldSelectedLocationLayer !== null){
                this.updateLocationLayerStyle(oldSelectedLocationLayer);
            }
            this.updateLocationLayerStyle(this.selectedLocationLayer);
        }
    }
    
    // unselects a location layer if it is selected (has no effect otherwise)
    unselectLocationLayer(layer){
        if(layer === this.selectedLocationLayer){
            this.selectedLocationLayer = null;
            this.updateLocationLayerStyle(layer);
        }
    }
    
    // updates the style of an address layer
    updateAddressLayerStyle(addressLayer){
        if(addressLayer === this.selectedLocationLayer){
            addressLayer.setStyle({"color": "#000000", "weight": 1, "fillColor": "#f4764b", "fillOpacity": 1});
        }
        else if(addressLayer.feature.properties.match === true){
            addressLayer.setStyle({"color": "#000000", "weight": 1, "fillColor": "#f4e542", "fillOpacity": 1});
        }
        else if(addressLayer.feature.properties.entities.size !== 0){
            addressLayer.setStyle({"color": "#000000", "weight": 1, "fillColor": "#806e75", "fillOpacity": 1});
        }
        else{
            addressLayer.setStyle({"color": "#000000", "weight": 1, "fillColor": "#9f9f9f", "fillOpacity": 1});
        }
    }
    
    // updates a location layer style
    updateLocationLayerStyle(layer){
        if(layer.feature.properties.type === 0){
            this.updatePlotLayerStyle(layer);
        }
        else if(layer.feature.properties.type === 1){
            this.updateAddressLayerStyle(layer);
        }
    }
    
    // updates the style of a plot layer
    updatePlotLayerStyle(plotLayer){
        if(plotLayer === this.selectedLocationLayer){
            plotLayer.setStyle({"color": "#555555", "weight": 1, "fillColor": "#f7541d", "fillOpacity": 0.5});
        }
        else if(plotLayer.feature.properties.match === true){
            plotLayer.setStyle({"color": "#555555", "weight": 1, "fillColor": "#b932b7", "fillOpacity": 0.5});
        }
        else if(plotLayer.feature.properties.entities.size !== 0){
            plotLayer.setStyle({"color": "#555555", "weight": 1, "fillColor": "#806e75", "fillOpacity": 0.5});
        }
        else{
            plotLayer.setStyle({"color": "#555555", "weight": 1, "fillColor": "#9f9f9f", "fillOpacity": 0.3});
        }
    }
    
    // updates locations displayed on the map depending on the map center and zoom level and on eventual search parameters
    updateLocations(){
        // display locations
        if(this.map.getZoom() >= this.locationsMinimumZoom){
            // not currently updating locations
            if(this.updatingLocations === false){
                // now updating locations
                this.updatingLocations = true;
                
                // craft the request URL
                const bounds = this.map.getBounds();
                let url = `/get_geolocated_locations.php?lat0=${bounds.getNorth()}&lat1=${bounds.getSouth()}&lng0=${bounds.getWest()}&lng1=${bounds.getEast()}`;
                if(this.locationSearch.currentSearch.string !== null){
                    url += `&search-type=${this.locationSearch.currentSearch.type.description}`;
                    url += `&search-string=${this.locationSearch.currentSearch.string}`;
                }
                
                // make the request
                fetch(url).then((response) => response.json()).then((json) => {
                    // the request matches too many locations
                    if(json["error"] === "too-many-locations"){
                        // display zoom warning
                        this.locationSearch.zoomWarning.style.display = "block";
                        
                        // remove location layers
                        for(const layerGroup of this.locationLayerGroups){
                            layerGroup.clearLayers();
                            this.selectedLocationLayer = null;
                        }
                    }
                    // the request doesn't match too many locations
                    else{
                        // hide zoom warning
                        this.locationSearch.zoomWarning.style.display = "none";
                        
                        // create maps of location layers that existed before this update, indexed by their ids
                        const previousLayers = new Array(
                            new Map(), // previous plot layers
                            new Map() // previous address layers
                        );
                        for(const locationType of [0, 1]){
                            for(const layer of this.locationLayerGroups[locationType].getLayers()){
                                previousLayers[locationType].set(layer.feature.properties.id, layer);
                                this.setLocationLayerMatchProperty(layer, false);
                            }
                        }
                        
                        // create a map of persisting location layers, which existed before and will exist after this update, indexed by their ids
                        const persistingLayers = new Array(
                            new Map(), // persisting plot layers
                            new Map() // persisting address layers
                        );
                        
                        // create a map of new location layers added during this update, indexed by their ids
                        const newLayers = new Array(
                            new Map(), // new plot layers
                            new Map() // new address layers
                        );
                        
                        // iterate locations sent by the server
                        for(const location of json["locations"]){
                            // set the entity, if any
                            let entity = null;
                            if(location.entity_number !== null){
                                entity = {
                                    "number": location.entity_number,
                                    "relation": location.entity_relation,
                                    "name": location.entity_name,
                                    "amount": location.entity_amount,
                                    "year": location.entity_year,
                                    "month": location.entity_month,
                                    "details": location.entity_details
                                }
                            }
                            
                            // get the previous layer matching the location, if any
                            const previousLayer = previousLayers[location.type].get(location.id);
                            
                            // if a previous layer matches the location
                            if(previousLayer !== undefined){
                                // update the layer match property
                                if(location.match === 1){
                                    this.setLocationLayerMatchProperty(previousLayer, true);
                                }
                                
                                // the layer existed before and will exist after this update
                                persistingLayers[location.type].set(location.id, previousLayer);
                            }
                            // else
                            else{
                                // get the new layer matching the location, if any
                                const newLayer = newLayers[location.type].get(location.id);
                                
                                // if a new layer matches the location
                                if(newLayer !== undefined){
                                    // update the layer match property
                                    if(location.match === 1){
                                        newLayer.properties.match = true;
                                    }
                                    
                                    // add the entity to the layer
                                    if(entity !== null){
                                        const entities_array = newLayer.properties.entities.get(entity.number);
                                        if(entities_array !== undefined){
                                            entities_array.push(entity);
                                        }
                                        else{
                                            newLayer.properties.entities.set(entity.number, new Array(entity));
                                        }
                                    }
                                }
                                // else
                                else{
                                    // add the new layer as a plot
                                    if(location.type === 0){
                                        newLayers[0].set(location.id, {
                                            "geometry": JSON.parse(location.plot_geography),
                                            "properties": {
                                                "id": location.id,
                                                "departement": location.departement,
                                                "type": 0,
                                                "commune": location.plot_commune,
                                                "prefixe": location.plot_prefixe,
                                                "section": location.plot_section,
                                                "numero": location.plot_numero,
                                                "communeName": location.plot_commune_name,
                                                "entities": entity !== null? new Map([[entity.number, new Array(entity)]]) : new Map(),
                                                "match": location.match === 1? true : false
                                            },
                                            "type": "Feature"
                                        });
                                    }
                                    // add the new layer as an address
                                    else if(location.type === 1){
                                        newLayers[1].set(location.id, {
                                            "geometry": JSON.parse(location.address_geography),
                                            "properties": {
                                                "id": location.id,
                                                "departement": location.departement,
                                                "type": 1,
                                                "text": location.address_text,
                                                "entities": entity !== null? new Map([[entity.number, new Array(entity)]]) : new Map(),
                                                "match": location.match === 1? true : false
                                            },
                                            "type": "Feature"
                                        });
                                    }
                                }
                            }
                        }
                        
                        // effectively add new layers
                        for(const locationType of [0, 1]){
                            for(const newLayer of newLayers[locationType].values()){
                                this.locationLayerGroups[locationType].addData(newLayer);
                            }
                        }

                        // remove previous layers which are no longer visible
                        for(const locationType of [0, 1]){
                            for(const [id, layer] of previousLayers[locationType]){
                                if(!persistingLayers[locationType].has(id)){
                                    if(layer === this.selectedLocationLayer){
                                        this.selectedLocationLayer = null;
                                    }
                                    this.locationLayerGroups[locationType].removeLayer(layer);
                                }
                            }
                        }
                    }
                    
                    // not updating locations anymore
                    this.updatingLocations = false;
                    if(this.chainLocationsUpdate === true){
                        this.chainLocationsUpdate = false;
                        this.updateLocations();
                    }
                }).catch((error) => {
                    // not updating locations anymore
                    this.updatingLocations = false;
                    if(this.chainLocationsUpdate === true){
                        this.chainLocationsUpdate = false;
                        this.updateLocations();
                    }
                });
            }
            // already updating locations
            else{
                this.chainLocationsUpdate = true;
            }
        }
        // don't display locations
        else{
            for(const layerGroup of this.locationLayerGroups){
                layerGroup.clearLayers();
                this.selectedLocationLayer = null;
            }
        }
    }
}

/* BottomLinks */

L.Control.BottomLinks = L.Control.extend({
    // called when the control is added
    onAdd: function(map){
        this.bottomLinks = L.DomUtil.create("div", "", this.container);
        this.bottomLinks.id = "bottom-links";
        
        squatMap.aboutWindow.createLink(this.bottomLinks);
        L.DomUtil.create("span", "", this.bottomLinks).innerHTML = " · ";
        
        squatMap.faqWindow.createLink(this.bottomLinks);
        L.DomUtil.create("span", "", this.bottomLinks).innerHTML = " · ";
        
        squatMap.warningWindow.createLink(this.bottomLinks);
        
        return this.bottomLinks;
    }
});

L.control.bottomLinks = function(opts){
    return new L.Control.BottomLinks(opts);
}

/* LanguageSwitcher */

L.Control.LanguageSwitcher = L.Control.extend({
    // called when the control is added
    onAdd: function(map){
        this.languageSwitcher = L.DomUtil.create("div", "language-switcher");
        L.DomEvent.disableClickPropagation(this.languageSwitcher);
        
        this.frButton = L.DomUtil.create("div", "language-switcher-button language-switcher-button-selected", this.languageSwitcher);
        this.frButton.id = "language-fr";
        this.frButton.innerText = "FR";
        L.DomEvent.on(this.frButton, "click", this.switchLanguage.bind(this));
        
        this.enButton = L.DomUtil.create("div", "language-switcher-button", this.languageSwitcher);
        this.enButton.id = "language-en";
        this.enButton.innerText = "EN";
        L.DomEvent.on(this.enButton, "click", this.switchLanguage.bind(this));
        
        return this.languageSwitcher;
    },
    
    // called when the language switches
    switchLanguage(event){
        if(!event.target.classList.contains("language-switcher-button-selected")){
            for(const element of Array.from(document.getElementsByClassName("language-switcher-button-selected"))){
                element.classList.remove("language-switcher-button-selected");
            }
            event.target.classList.add("language-switcher-button-selected");
            
            for(const cssStylesheet of Array.from(document.styleSheets)){
                if(cssStylesheet.href !== null && cssStylesheet.href.includes("map.css")){
                    for(const cssRule of cssStylesheet.cssRules){
                        if(cssRule.selectorText === "." + event.target.id){
                            cssRule.style.display = "inline";
                        }
                        else if(cssRule.selectorText === ".language-fr" || cssRule.selectorText === ".language-en"){
                            cssRule.style.display = "none";
                        }
                    }
                }
            }
        }
    }
});

L.control.languageSwitcher = function(opts){
    return new L.Control.LanguageSwitcher(opts);
}

/* TileLayerSwitcher */

L.Control.TileLayerSwitcher = L.Control.extend({
    // called when the control is added
    onAdd: function(map){
        this.tileLayerSwitcher = L.DomUtil.create("div", "tile-layer-switcher");
        L.DomEvent.disableClickPropagation(this.tileLayerSwitcher);
        
        this.openStreetMapButton = L.DomUtil.create("div", "tile-layer-switcher-button tile-layer-switcher-button-selected", this.tileLayerSwitcher);
        this.openStreetMapButton.id = "tile-layer-openstreetmap";
        this.openStreetMapButton.innerText = "OpenStreetMap";
        L.DomEvent.on(this.openStreetMapButton, "click", this.switchTileLayer.bind(this));
        
        this.firstSatelliteButton = L.DomUtil.create("div", "tile-layer-switcher-button", this.tileLayerSwitcher);
        this.firstSatelliteButton.id = "tile-layer-first-satellite";
        this.firstSatelliteButton.innerText = "Satellite (Esri)";
        L.DomEvent.on(this.firstSatelliteButton, "click", this.switchTileLayer.bind(this));
        
        this.secondSatelliteButton = L.DomUtil.create("div", "tile-layer-switcher-button", this.tileLayerSwitcher);
        this.secondSatelliteButton.id = "tile-layer-second-satellite";
        this.secondSatelliteButton.innerText = "Satellite (Google)";
        L.DomEvent.on(this.secondSatelliteButton, "click", this.switchTileLayer.bind(this));
        
        return this.tileLayerSwitcher;
    },
    
    // called when the tile layer switches
    switchTileLayer(event){
        if(!event.target.classList.contains("tile-layer-switcher-button-selected")){
            if((event.target.id === "tile-layer-first-satellite" || event.target.id === "tile-layer-second-satellite") && squatMap.warningWindowDisplayed === false){
                squatMap.warningWindowDisplayed = true;
                squatMap.warningWindow.show();
            }
            else{
                for(const element of Array.from(document.getElementsByClassName("tile-layer-switcher-button-selected"))){
                    element.classList.remove("tile-layer-switcher-button-selected");
                }
                event.target.classList.add("tile-layer-switcher-button-selected");
                
                if(event.target.id === "tile-layer-openstreetmap"){
                    squatMap.map.removeLayer(squatMap.firstSatellite);
                    squatMap.map.removeLayer(squatMap.secondSatellite);
                    squatMap.map.addLayer(squatMap.openStreetMap);
                }
                else if(event.target.id === "tile-layer-first-satellite"){
                    squatMap.map.removeLayer(squatMap.openStreetMap);
                    squatMap.map.removeLayer(squatMap.secondSatellite);
                    squatMap.map.addLayer(squatMap.firstSatellite);
                }
                else if(event.target.id === "tile-layer-second-satellite"){
                    squatMap.map.removeLayer(squatMap.openStreetMap);
                    squatMap.map.removeLayer(squatMap.firstSatellite);
                    squatMap.map.addLayer(squatMap.secondSatellite);
                }
            }
        }
    }
});

L.control.tileLayerSwitcher = function(opts){
    return new L.Control.TileLayerSwitcher(opts);
}

/* PopupWindow */

L.Control.PopupWindow = L.Control.extend({
    onAdd: function(map) {
        this.container = L.DomUtil.create("div", "popup-window-container");
        L.DomEvent.on(this.container, "click", this._containerClicked, this);
        L.DomEvent.on(
            this.container,
            "mousedown dblclick mouseup mousemove contextmenu keypress keydown keyup preclick wheel",
            function(event){ event.stopPropagation(); }
        );
        
        this.window = L.DomUtil.create("div", "popup-window-window", this.container);
        this.window.innerHTML = this.options.content;
        
        this.closeButton = L.DomUtil.create("button", "popup-window-close-button", this.window);
        this.closeButton.type = "button";
        this.closeButton.innerHTML = "×";
        L.DomEvent.on(this.closeButton, "click", this.hide, this);

        return this.container;
    },
    
    _containerClicked: function(event){
        if(event.target === this.container){
            this.hide();
        }
        event.stopPropagation();
    },
    
    createLink: function(parent){
        const link = L.DomUtil.create("button", "popup-window-link", parent);
        L.DomEvent.on(link, "click", this.show, this);
        link.innerHTML = this.options.linkText;
        return link;
    },
    
    hide: function(){
        this.container.style.display = "none";
    },
    
    show: function(){
        this.container.style.display = "flex";
    }
});

L.control.popupWindow = function(opts) {
    return new L.Control.PopupWindow(opts);
}

/* Create the map */

let squatMap = new SquatMap();
squatMap.initialize();
