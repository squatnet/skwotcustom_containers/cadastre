<?php
    class SuggestionsQuery{
        public $parameters = [];
        public $sqls = [];
        
        // constructor
        public function __construct(){
            // if a search string was provided
            if(!empty($_GET['search-string']) && is_string($_GET['search-string'])){
                # craft the FTS5 query following the syntax at https://www.sqlite.org/fts5.html#full_text_query_syntax
                $fts5Query = "";
                foreach(explode(" ", $_GET['search-string']) as $term){
                    // terms of at least three characters are used as-is
                    if(strlen($term) >= 3){
                        $fts5Query = $fts5Query . "\"$term\" ";
                    }
                    // terms of less than three characters beginning with a digit are presumed to be address numbers
                    // they are prefixed and suffixed with spaces, e.g. address number "1" will become the term " 1 "
                    // this works because address texts in the database are prefixed by a space, precisely to make this work
                    else if(ctype_digit($term[0])){
                        $fts5Query = $fts5Query . "\" $term \" ";
                    }
                    // other terms of less than three characters are ignored
                }
                
                // craft the SQL queries
                if($fts5Query !== ""){
                    // add the FTS5 query to parameters
                    array_push($this->parameters, $fts5Query);
                    
                    // craft the address query
                    // both the highlighted text (without diacritics) and the original text (with diacritics) are returned
                    // note that the highlighted text, in addition to the <b> tags, is prefixed by a space (possibly inside the first set of <b> tags)
                    array_push(
                        $this->sqls,
                        sprintf(
                            "select highlight(address, 0, '<b>', '</b>') as highlighted_text, original_text from address where text match $%d limit 8;",
                            sizeof($this->parameters)
                        )
                    );
                    
                    // craft the entity query
                    array_push($this->sqls, sprintf("select highlight(entity, 0, '<b>', '</b>') as name, number, plots, addresses from entity where name match $%d limit 8;", sizeof($this->parameters)));
                }
            }
        }
        
        // opens a new database connection, executes the queries and returns the result as a JSON string
        public function execute(){
            // connect to the database
            $connection = new SQLite3('../../database.db', SQLITE3_OPEN_READONLY);
            
            // execute the requests
            $addresses = array();
            $entities = array();
            foreach($this->sqls as $sqlIndex => $sql){
                // prepare the statement
                $statement = $connection->prepare($sql);
                foreach($this->parameters as $index => $parameter){
                    $statement->bindValue($index + 1, $parameter);
                }
                
                // execute the statement
                $result = $statement->execute();
                
                // get the suggestions
                while($suggestion = $result->fetchArray(SQLITE3_ASSOC)){
                    if($sqlIndex === 0){
                        array_push($addresses, $suggestion);
                    }
                    else if($sqlIndex === 1){
                        array_push($entities, $suggestion);
                    }
                }
            }
            
            // close the database connection
            $connection->close();
            
            // return the suggestions
            return json_encode(array("addresses" => $addresses, "entities" => $entities));
        }
    }
?>

